import requests

from typing import Union, Dict, List, Optional
from urllib.parse import unquote


def extract_href(obj: Dict) -> str:
    if "href" in obj:
        if isinstance(obj["href"], str):
            return obj["href"]
    if "self" in obj and "href" in obj["self"]:
        return extract_href(obj["self"])
    if "id" in obj and "href" in obj["id"]:
        return extract_href(obj["id"])
    raise Exception("Missing href in object")


def extract_id(identifier: Union[Dict, str]) -> str:
    if isinstance(identifier, dict):
        href = extract_href(identifier)
    else:
        href = identifier
    return unquote(href.split("/")[-1])


def split_id(identifier: str) -> List[str]:
    return identifier.split(",")
