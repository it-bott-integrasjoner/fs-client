"""
Utilities for semesters and terms in FS
"""

import copy
import datetime
import enum
import time

from typing import Optional


class Semesters(enum.Enum):
    # Order is important here
    SPRING = "VÅR"
    SUMMER = "SOM"
    AUTUMN = "HØST"


def date_to_semester(date):
    """Convert a date to either a SPRING or AUTUMN semester."""
    # Spring: 01-01 -> 07-31
    # Autumn: 08-01 -> 12-31
    if date.month > 7:
        return Semesters.AUTUMN
    return Semesters.SPRING


class Term:
    """
    Term objects represents terms in multiterm subjects.

    A term object represents how terms are ordered and sequenced, and
    calculates the term data of the next and previous terms *would* be. A given
    term object may not exist in the FS database.
    """

    def __init__(self, year, semester, number=None):
        """
        :param int year: The year for this term
        :type semester: str, Semester
        :param semester: The semester name of this term
        :param int number: The term number in a sequence of related terms
        """
        self.year = int(year)
        self.semester = Semesters(semester)
        self.number = int(number) if number is not None else None

    def __repr__(self):
        return (
            "{cls.__name__}" "({obj.year:04d}," " {obj.semester!r}," " {obj.number!r})"
        ).format(cls=type(self), obj=self)

    def __copy__(self):
        return type(self)(self.year, self.semester, self.number)

    def __eq__(self, other):
        attrs = ("year", "semester", "number")
        if not all(hasattr(other, attr) for attr in attrs):
            return NotImplemented
        return all(getattr(self, attr) == getattr(other, attr) for attr in attrs)

    def prev_term(self):
        """
        Get the previous term.

        :rtype: Term, None
        :return: The previous term, if any.
        """
        t = copy.copy(self)
        if t.number == 1:
            return None
        if t.number is not None:
            t.number -= 1
        if t.semester == Semesters.SPRING:
            t.year -= 1
            t.semester = Semesters.AUTUMN
        elif t.semester == Semesters.AUTUMN:
            t.semester = Semesters.SPRING
        elif t.semester == Semesters.SUMMER:
            return None
        else:
            raise ValueError(f"Unhandled semester {self.semester!r}")
        return t

    def next_term(self):
        """
        Get the next term.

        :rtype: Term
        :return: The next term.
        """
        t = copy.copy(self)
        if t.number is not None:
            t.number += 1
        if t.semester == Semesters.SPRING:
            t.semester = Semesters.AUTUMN
        elif t.semester == Semesters.AUTUMN:
            t.year += 1
            t.semester = Semesters.SPRING
        elif t.semester == Semesters.SUMMER:
            return None
        else:
            raise ValueError(f"Unhandled semester {self.semester!r}")
        return t


class TermIterator:
    """
    Iterate over terms.

    .. caution::
        This iterator may return an infinite sequence of terms.
    """

    def __init__(self, term, reverse=False):
        """
        :type term: Term
        :param term: The initial term
        :param reverse: Look for terms previous to the initial term
        """
        self.cur = term
        self.reverse = reverse

    def __repr__(self):
        return "{cls.__name__}({obj.cur!r})".format(cls=type(self), obj=self)

    def __iter__(self):
        return self

    def __next__(self):
        if self.cur is None:
            raise StopIteration()
        term = self.cur
        if self.reverse:
            self.cur = term.prev_term()
        else:
            self.cur = term.next_term()
        return term


class PeriodTermIterator:
    """
    Iterate over terms in a given date period.
    """

    def __init__(self, start=None, end=None):
        """
        :param date/str start: The start date
        :param date/str end: The end date
        """
        self.start = self.convert_to_date(start)
        self.end = self.convert_to_date(end)
        first_date = self.start or self.end
        if first_date is None:
            raise ValueError("Must have at least one date")
        self.cur = Term(
            year=first_date.year, semester=date_to_semester(first_date), number=1
        )
        self.prev = None
        self.last_date = self.end or self.start

    def __repr__(self):
        return (
            "{cls.__name__}" "({obj.start!r}," " {obj.end!r})," " cur={obj.cur!r}"
        ).format(cls=type(self), obj=self)

    def __iter__(self):
        return self

    def __next__(self):
        if self.cur is None or (
            self.prev
            and self.prev.year == self.last_date.year
            and self.prev.semester == date_to_semester(self.last_date)
        ):
            raise StopIteration()
        self.prev = self.cur
        term = self.cur
        self.cur = term.next_term()
        return term

    def convert_to_date(self, value):
        if not value or isinstance(value, datetime.date):
            return value
        return datetime.datetime.strptime(value, "%Y-%m-%d")


def term_to_shorthand(term: Term) -> str:
    return "{year}{semester}".format(
        year=str(term.year)[2:],
        semester=term.semester.value[:1],
    )


def expand_single_letter_semester(l: str) -> Optional[str]:
    l = l.upper()
    return {
        "V": Semesters.SPRING.value,
        "H": Semesters.AUTUMN.value,
        "S": Semesters.SUMMER.value,
    }.get(l)


def shorthand_to_term(shorthand: str) -> Optional[Term]:
    assert len(shorthand) == 3
    two_digit_year, semester_short = shorthand[:2], shorthand[-1:]
    # values 69-99 are mapped to 1969-1999, and values 0-68 are mapped to 2000-2068
    year = time.strptime(two_digit_year, "%y").tm_year
    semester = expand_single_letter_semester(semester_short)
    if not year or not semester:
        return
    return Term(year, semester, number=None)
