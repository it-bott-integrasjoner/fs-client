import collections.abc
import datetime
import logging
import urllib.parse
import requests

from typing import Any, Dict, Optional, Union, List, Tuple, Type, Iterator, Generic
from urllib.parse import quote, urljoin, urlparse

from .models import (
    Aktivitet,
    AktivitetId,
    Deltaker,
    DeltakerId,
    Emne,
    EmneId,
    EvuKurs,
    EvuKursId,
    EvuKursDeltakelse,
    EvuKursDeltakelseId,
    FsIdentifier,
    FsObject,
    Kull,
    KullId,
    KullKlasse,
    KullKlasseId,
    KullKlasseStudent,
    KullKlasseStudentId,
    Organisasjonsenhet,
    Person,
    PersonId,
    PersonRolle,
    PersonRolleId,
    Rolle,
    RolleId,
    Semesterregistrering,
    SemesterregistreringId,
    StudentAktivitet,
    StudentAktivitetId,
    StudentUndervisning,
    StudentUndervisningId,
    StudentVurdering,
    StudentVurderingId,
    Studieprogram,
    StudieprogramId,
    Studierett,
    StudierettId,
    Undervisning,
    UndervisningId,
    VurderingsResultatStatus,
    VurderingsEnhet,
    VurderingsEnhetId,
    VurderingsKombinasjon,
    VurderingsKombinasjonId,
    VurderingsTid,
)
from .utils import extract_id, extract_href


logger = logging.getLogger(__name__)


DEFAULT_USE_MODELS = True


def quote_path_arg(arg: Union[str, int]):
    return urllib.parse.quote(str(arg), safe=",")


class FsEndpoints:
    """ Get endpoints from an API base URL. """

    def __init__(self, url: str):
        self.baseurl = url

    def __repr__(self):
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def add_base(self, path: str):
        return urljoin(self.baseurl, path)

    def get_token(self):
        return urljoin(self.baseurl, "token")

    def get_emne(self, emne_id: str):
        fs_id = quote_path_arg(emne_id)
        return urljoin(self.baseurl, f"emner/{fs_id}")

    def list_emner(self):
        return urljoin(self.baseurl, "emner")

    def get_undervisningsaktivitet(self, undervisningsaktivitet_id: str):
        fs_id = quote_path_arg(undervisningsaktivitet_id)
        return urljoin(self.baseurl, f"undervisningsaktiviteter/{fs_id}")

    def list_undervisningsaktiviteter(self):
        return urljoin(self.baseurl, "undervisningsaktiviteter")

    def get_undervisning(self, undervisning_id: str):
        fs_id = quote_path_arg(undervisning_id)
        return urljoin(self.baseurl, f"undervisning/{fs_id}")

    def list_undervisninger(self):
        return urljoin(self.baseurl, "undervisning")

    def get_studentundervisning(self, studentundervisning_id: str):
        fs_id = quote_path_arg(studentundervisning_id)
        return urljoin(self.baseurl, f"studentundervisning/{fs_id}")

    def list_studentundervisninger(self):
        return urljoin(self.baseurl, "studentundervisning")

    def get_studentundervisningsaktivitet(self, studentundervisningsaktivitet_id: str):
        fs_id = quote_path_arg(studentundervisningsaktivitet_id)
        return urljoin(self.baseurl, f"studentundervisningsaktiviteter/{fs_id}")

    def list_studentundervisningsaktiviteter(self):
        return urljoin(self.baseurl, "studentundervisningsaktiviteter")

    def get_person(self, person_id: Union[str, int]):
        fs_id = quote_path_arg(person_id)
        return urljoin(self.baseurl, f"personer/{fs_id}")

    def list_personer(self):
        return urljoin(self.baseurl, "personer")

    def get_deltaker(self, deltaker_id: Union[str, int]):
        fs_id = quote_path_arg(deltaker_id)
        return urljoin(self.baseurl, f"deltakere/{fs_id}")

    def list_deltakere(self):
        return urljoin(self.baseurl, "deltakere")

    def get_rolle(self, rolle_id: str):
        fs_id = quote_path_arg(rolle_id)
        return urljoin(self.baseurl, f"roller/{fs_id}")

    def list_roller(self):
        return urljoin(self.baseurl, "roller")

    def get_personrolle(self, personrolle_id: str):
        fs_id = quote_path_arg(personrolle_id)
        return urljoin(self.baseurl, f"personroller/{fs_id}")

    def list_personroller(self):
        return urljoin(self.baseurl, "personroller")

    def list_evukurs(self):
        return urljoin(self.baseurl, "evukurs")

    def get_evukurs(self, evukurs_id: str):
        fs_id = quote_path_arg(evukurs_id)
        return urljoin(self.baseurl, f"evukurs/{fs_id}")

    def list_evukursdeltakelser(self):
        return urljoin(self.baseurl, "evukursdeltakelser")

    def get_evukursdeltakelse(self, evukursdeltakelse_id: str):
        fs_id = quote_path_arg(evukursdeltakelse_id)
        return urljoin(self.baseurl, f"evukursdeltakelser/{fs_id}")

    def list_vurderingskombinasjoner(self):
        return urljoin(self.baseurl, "vurderingskombinasjoner")

    def get_vurderingskombinasjon(self, vurderingskombinasjon_id: str):
        fs_id = quote_path_arg(vurderingskombinasjon_id)
        return urljoin(self.baseurl, f"vurderingskombinasjoner/{fs_id}")

    def get_vurderingsenhet(self, vurderingsenhet_id: str):
        fs_id = quote_path_arg(vurderingsenhet_id)
        return urljoin(self.baseurl, f"vurderingsenheter/{fs_id}")

    def list_vurderingsenheter(self):
        return urljoin(self.baseurl, "vurderingsenheter")

    def list_studentvurderinger(self):
        return urljoin(self.baseurl, "studentvurderinger")

    def get_studentvurdering(self, studentvurdering_id: str):
        fs_id = quote_path_arg(studentvurdering_id)
        return urljoin(self.baseurl, f"studentvurderinger/{fs_id}")

    def get_vurderingsresultatstatuser(self, resultat_id: str):
        fs_id = quote_path_arg(resultat_id)
        return urljoin(self.baseurl, f"vurderingsresultatstatuser/{fs_id}")

    def get_vurderingstid(self, vurderingstid_id: str):
        fs_id = quote_path_arg(vurderingstid_id)
        return urljoin(self.baseurl, f"vurderingstider/{fs_id}")

    def get_studieprogram(self, studieprogram_id: str):
        fs_id = quote_path_arg(studieprogram_id)
        return urljoin(self.baseurl, f"studieprogrammer/{fs_id}")

    def list_studieprogrammer(self):
        return urljoin(self.baseurl, "studieprogrammer")

    def get_studierett(self, studierett_id: str):
        fs_id = quote_path_arg(studierett_id)
        return urljoin(self.baseurl, f"studieretter/{fs_id}")

    def list_studieretter(self):
        return urljoin(self.baseurl, "studieretter")

    def get_semesterregistrering(self, semesterregistrering_id: str):
        fs_id = quote_path_arg(semesterregistrering_id)
        return urljoin(self.baseurl, f"semesterregistreringer/{fs_id}")

    def list_semesterregistreringer(self):
        return urljoin(self.baseurl, "semesterregistreringer")

    def get_kull(self, kull_id: str):
        fs_id = quote_path_arg(kull_id)
        return urljoin(self.baseurl, f"kull/{fs_id}")

    def list_kull(self):
        return urljoin(self.baseurl, "kull")

    def get_kullklasse(self, kullklasse_id: str):
        fs_id = quote_path_arg(kullklasse_id)
        return urljoin(self.baseurl, f"kullklasser/{fs_id}")

    def list_kullklasser(self):
        return urljoin(self.baseurl, "kullklasser")

    def get_kullklassestudent(self, kullklassestudent_id: str):
        fs_id = quote_path_arg(kullklassestudent_id)
        return urljoin(self.baseurl, f"kullklassestudenter/{fs_id}")

    def list_kullklassestudenter(self):
        return urljoin(self.baseurl, "kullklassestudenter")

    def get_organisasjonsenhet(self, organisasjonsenhet_id: str):
        fs_id = quote_path_arg(organisasjonsenhet_id)
        return urljoin(self.baseurl, f"organisasjonsenheter/{fs_id}")

    def list_organisasjonsenheter(self):
        return urljoin(self.baseurl, "organisasjonsenheter")


class FsParams(collections.abc.Mapping):
    """
    Transform `FsIdentifier` field names to parameters that can be used to
    filter results on various endpoints.

    This is a hacky solution. Please remove when figuring out how to implement
    this properly.
    """

    def __init__(self, *args, **kwargs: Dict[str, str]):
        self._transforms: Dict[str, str] = dict(*args, **kwargs)

    def __getitem__(self, item: str):
        return self._transforms[item]

    def __iter__(self):
        return iter(self._transforms)

    def __len__(self):
        return len(self._transforms)

    def __call__(self, d: Dict[str, str]):
        new: Dict[str, str] = dict()
        for k in d:
            mod_k = k.replace("_", ".")
            mod_k = self.get(mod_k, mod_k)
            new[mod_k] = d[k]
        return new


class FsClient:
    def __init__(
        self,
        url: str,
        headers: Optional[Dict[str, str]] = None,
        rewrite_url: Optional[Tuple[str, str]] = None,
        use_models: bool = DEFAULT_USE_MODELS,
        use_sessions: bool = True,
        rewrite_relative_urls: bool = False,
    ):
        """
        FS API client.

        :param str url: Base API URL
        :param dict headers: Append extra headers to all requests
        :param tuple(str, str) rewrite_url: Rewrite URLs before sending requests (from, to)
        :param bool use_models:
            When the API returns data or a list of identifiers,
            convert to objects as defined in fs_client.models
        :param bool use_sessions:
            Re-use connections (default: on)
        :param bool rewrite_relative_urls:
            Rewrite relative URLs in FS-API JSON responses (default: off)
        """
        self.urls = FsEndpoints(url)
        self.headers = {} if headers is None else headers
        self.rewrite_url: Optional[Tuple[str, str]] = rewrite_url
        self.use_models: bool = use_models
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests
        self.rewrite_relative_urls = rewrite_relative_urls

    def _build_request_headers(
        self, headers: Optional[Dict[Any, Any]]
    ) -> Dict[Any, Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        for h in headers or ():
            request_headers[h] = headers[h]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, str]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: bool = False,
        **kwargs,
    ):
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        if self.rewrite_url is not None:
            url = url.replace(*self.rewrite_url)
        logger.debug(
            "Calling %s %s with params=%r", method_name, urlparse(url).path, params
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if r.status_code in (500, 400, 401):
            logger.warning("Got HTTP %d: %r", r.status_code, r.content)
        if return_response:
            return r
        r.raise_for_status()
        return r.json()

    def get(self, url: str, **kwargs):
        response = self.call("GET", url, **kwargs)
        if self.rewrite_relative_urls and isinstance(response, dict):
            self.rewrite_relative_urls_recursive(response, "href")
        return response

    def rewrite_relative_urls_recursive(self, response: Dict[str, Any], url_field: str):
        """
        Use this method to rewrite relative to absolute urls in responses.

        Method uses recursion to search in nested list/dict.

        :response: The dictionary to search
        :url: The URL field to look for
        """
        for _, value in response.items():
            if isinstance(value, dict):
                # If value is a dict do recursive checking of values
                self.rewrite_relative_urls_recursive(value, url_field)
            elif isinstance(value, list):
                # Check lists. If they contain dict values do recursive check for values
                for item in value:
                    if isinstance(item, dict):
                        self.rewrite_relative_urls_recursive(item, url_field)
        if url_field in response and response[url_field].startswith("/"):
            # If dict has specified fieldname, and it contains a relative url
            # Prepend with clients base_url
            response[url_field] = self.urls.add_base(response[url_field])

    def post(self, url: str, **kwargs):
        return self.call("POST", url, **kwargs)

    def put(self, url: str, **kwargs):
        return self.call("PUT", url, **kwargs)

    def depaginate(self, page: Dict[str, Any]):
        """ Yield all items, following links from `page`. """
        while page:
            for item in page.get("items", []):
                yield item
            next_page = page.get("next", {}).get("href")
            if next_page:
                page: Dict[str, Any] = self.get(next_page)
            else:
                break

    def str_or_model(
        self, identifier: str, object_type: Type[FsIdentifier]
    ) -> Union[FsIdentifier, dict]:
        fs_id = extract_id(identifier)
        if not (self.use_models and object_type):
            return fs_id
        if isinstance(identifier, dict) and "id" in identifier:
            return object_type(**identifier["id"])
        return object_type.from_fs_id(fs_id)

    def dict_or_model(self, obj: dict, object_type: Type[FsObject]):
        if not (self.use_models and object_type):
            return obj
        return object_type.from_dict(obj)

    def list_emner(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Emne, EmneId, dict]]:
        """ /emner """
        fs_params = FsParams({})
        params = {"limit": limit}
        params.update(fs_params(kwargs))
        url = self.urls.list_emner()
        emner = self.get(url, params=params)
        for emne in self.depaginate(emner):
            href = extract_href(emne)
            if expand:
                emne_dict = self.get(href)
                yield self.dict_or_model(emne_dict, Emne)
            else:
                yield self.str_or_model(href, EmneId)

    def get_emne(self, emne_id: str) -> Optional[Union[Emne, dict]]:
        """ /emner/{id} """
        url = self.urls.get_emne(emne_id)
        try:
            emne = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(emne, Emne)

    def list_undervisninger(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Undervisning, UndervisningId, dict]]:
        """ /undervisning """
        fs_params = FsParams(
            {
                "undervisningsenhet.terminnummer": "terminnummer",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_undervisninger()
        undenheter = self.get(url, params=params)
        for undenh in self.depaginate(undenheter):
            href = extract_href(undenh)
            if expand:
                undervisning_dict = self.get(href)
                yield self.dict_or_model(undervisning_dict, Undervisning)
            else:
                yield self.str_or_model(href, UndervisningId)

    def get_undervisning(
        self, undervisning_id: str
    ) -> Optional[Union[Undervisning, dict]]:
        """ /undervisning/{id} """
        url = self.urls.get_undervisning(undervisning_id)
        try:
            undervisning = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(undervisning, Undervisning)

    def list_undervisningsaktiviteter(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Aktivitet, AktivitetId, dict]]:
        """ /undervisningersaktiviteter """
        fs_params = FsParams(
            {
                "emne.institusjon": "undervisning.emne.institusjon",
                "emne.kode": "undervisning.emne.kode",
                "emne.versjon": "undervisning.emne.versjon",
                "semester.ar": "undervisning.semester.ar",
                "semester.termin": "undervisning.semester.termin",
                "undervisningsenhet.terminnummer": "undervisning.terminnummer",
                "undervisningsaktivitet.aktivitet": "aktivitet",
            }
        )

        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_undervisningsaktiviteter()
        aktiviteter = self.get(url, params=params)
        for aktivitet in self.depaginate(aktiviteter):
            href = extract_href(aktivitet)
            if expand:
                aktivitet_dict = self.get(href)
                yield self.dict_or_model(aktivitet_dict, Aktivitet)
            else:
                yield self.str_or_model(href, AktivitetId)

    def get_undervisningsaktivitet(
        self, undervisningsaktivitet_id: str
    ) -> Optional[Union[Aktivitet, dict]]:
        """ /undervisningersaktiviteter/{id} """
        url = self.urls.get_undervisningsaktivitet(undervisningsaktivitet_id)
        try:
            aktivitet = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(aktivitet, Aktivitet)

    def list_studentundervisninger(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[StudentUndervisning, StudentUndervisningId, dict]]:
        """ /studentundervisning """
        fs_params = FsParams(
            {
                "emne.institusjon": "undervisning.emne.institusjon",
                "emne.kode": "undervisning.emne.kode",
                "emne.versjon": "undervisning.emne.versjon",
                "semester.ar": "undervisning.semester.ar",
                "semester.termin": "undervisning.semester.termin",
                "undervisningsenhet.terminnummer": "undervisning.terminnummer",
                "person.id": "person.personlopenummer",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_studentundervisninger()
        unds = self.get(url, params=params)
        for und in self.depaginate(unds):
            href = extract_href(und)
            if expand:
                und_dict = self.get(href)
                yield self.dict_or_model(und_dict, StudentUndervisning)
            else:
                yield self.str_or_model(href, StudentUndervisningId)

    def get_studentundervisning(
        self, studentundervisning_id: str
    ) -> Optional[Union[StudentUndervisning, dict]]:
        """ /studentundervisning/{id} """
        url = self.urls.get_studentundervisning(studentundervisning_id)
        try:
            studentundervisning = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(studentundervisning, StudentUndervisning)

    def list_studentundervisningsaktiviteter(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[StudentAktivitet, StudentAktivitetId, dict]]:
        """ /studentundervisningsaktiviteter """
        fs_params = FsParams(
            {
                "emne.institusjon": "undervisningsaktivitet.undervisning.emne.institusjon",
                "emne.kode": "undervisningsaktivitet.undervisning.emne.kode",
                "emne.versjon": "undervisningsaktivitet.undervisning.emne.versjon",
                "semester.ar": "undervisningsaktivitet.undervisning.semester.ar",
                "semester.termin": "undervisningsaktivitet.undervisning.semester.termin",
                "undervisningsenhet.terminnummer": "undervisningsaktivitet.undervisning.terminnummer",
                # 'undervisningsaktivitet.aktivitet':
                #     'undervisningsaktivitet.aktivitet',
                "person.id": "person.personlopenummer",
            }
        )

        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_studentundervisningsaktiviteter()
        akts = self.get(url, params=params)
        for akt in self.depaginate(akts):
            href = extract_href(akt)
            if expand:
                akt_dict = self.get(href)
                yield self.dict_or_model(akt_dict, StudentAktivitet)
            else:
                yield self.str_or_model(href, StudentAktivitetId)

    def get_studentundervisningsaktivitet(
        self, studentundervisningsaktivitet_id: str
    ) -> Optional[Union[StudentAktivitet, dict]]:
        """ /studentundervisningsaktiviteter/{id} """
        url = self.urls.get_studentundervisningsaktivitet(
            studentundervisningsaktivitet_id
        )
        try:
            studentundervisningsaktivitet = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(studentundervisningsaktivitet, StudentAktivitet)

    def list_personroller(
        self, expand: bool = False, **kwargs
    ) -> Iterator[Union[PersonRolle, PersonRolleId, dict]]:
        """ /personroller """
        fs_params = FsParams(
            {
                "emne.institusjon": "organisasjonsenhet.institusjon",
                "kull.semester.ar": "semester.ar",
                "kull.semester.termin": "semester.termin",
                "undervisningsenhet.terminnummer": "undervisning.terminnummer",
                "person.id": "person.personlopenummer",
            }
        )
        url = self.urls.list_personroller()
        params = {
            "limit": 100,
        }
        params.update(fs_params(kwargs))
        roles = self.get(url, params=params)
        for role in self.depaginate(roles):
            href = extract_href(role)
            if expand:
                role_dict = self.get(href)
                yield self.dict_or_model(role_dict, PersonRolle)
            else:
                yield self.str_or_model(href, PersonRolleId)

    def get_personrolle(
        self, person_id: Union[str, int], rollenummer: Union[str, int]
    ) -> Optional[Union[PersonRolle, dict]]:
        """ /personroller/{id} """
        url = self.urls.get_personrolle(f"{person_id},{rollenummer}")
        try:
            personrolle = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(personrolle, PersonRolle)

    def get_rolle(self, rolle_id: str) -> Optional[Union[Rolle, dict]]:
        """ /roller/{id} """
        url = self.urls.get_rolle(rolle_id)
        try:
            rolle = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(rolle, Rolle)

    def list_roller(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Rolle, RolleId, dict]]:
        """ /roller """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_roller()
        roller = self.get(url, params=params)
        for rolle in self.depaginate(roller):
            href = extract_href(rolle)
            if expand:
                rolle_dict = self.get(href)
                yield self.dict_or_model(rolle_dict, Rolle)
            else:
                yield self.str_or_model(href, RolleId)

    def get_person(self, person_id: Union[str, int]) -> Optional[Union[Person, dict]]:
        """ /personer/{id} """
        url = self.urls.get_person(person_id)
        try:
            person = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(person, Person)

    def list_personer(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Person, PersonId, dict]]:
        """ /personer """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_personer()
        personer = self.get(url, params=params)
        for person in self.depaginate(personer):
            href = extract_href(person)
            if expand:
                person_dict = self.get(href)
                yield self.dict_or_model(person_dict, Person)
            else:
                yield self.str_or_model(href, PersonId)

    def get_deltaker(
        self, deltaker_id: Union[str, int]
    ) -> Optional[Union[Deltaker, dict]]:
        """ /deltakere/{id} """
        url = self.urls.get_deltaker(deltaker_id)
        try:
            deltaker = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(deltaker, Deltaker)

    def list_deltakere(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[Deltaker, DeltakerId, dict]]:
        """ /deltakere """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_deltakere()
        deltakere = self.get(url, params=params)
        for deltaker in self.depaginate(deltakere):
            href = extract_href(deltaker)
            if expand:
                deltaker_dict = self.get(href)
                yield self.dict_or_model(deltaker_dict, Deltaker)
            else:
                yield self.str_or_model(href, DeltakerId)

    def list_evukurs(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[EvuKurs, EvuKursId, dict]]:
        """ /evukurs """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_evukurs()
        evukurs = self.get(url, params=params)
        for kurs in self.depaginate(evukurs):
            href = extract_href(kurs)
            if expand:
                evukurs_dict = self.get(href)
                yield self.dict_or_model(evukurs_dict, EvuKurs)
            else:
                yield self.str_or_model(href, EvuKursId)

    def get_evukurs(self, evukurs_id: str) -> Optional[Union[EvuKurs, dict]]:
        """ /evukurs/{id} """
        url = self.urls.get_evukurs(evukurs_id)
        try:
            evukurs = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(evukurs, EvuKurs)

    def list_evukursdeltakelser(
        self, limit: int = 100, expand: bool = False, **kwargs
    ) -> Iterator[Union[EvuKursDeltakelse, EvuKursDeltakelseId, dict]]:
        """ /evukursdeltakelser """
        fs_params = FsParams(
            {
                "kode": "evuKurs.kode",
                "tidsangivelse": "evuKurs.tidsangivelse",
                "person.id": "person.personlopenummer",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_evukursdeltakelser()
        evukursdeltakelser = self.get(url, params=params)
        for deltakelse in self.depaginate(evukursdeltakelser):
            href = extract_href(deltakelse)
            if expand:
                deltakelse_dict = self.get(href)
                yield self.dict_or_model(deltakelse_dict, EvuKursDeltakelse)
            else:
                yield self.str_or_model(href, EvuKursDeltakelseId)

    def get_evukursdeltakelse(
        self, evukursdeltakelse_id: str
    ) -> Optional[Union[EvuKursDeltakelse, dict]]:
        """ /evukursdeltakelser/{id} """
        url = self.urls.get_evukursdeltakelse(evukursdeltakelse_id)
        try:
            evukursdeltakelse = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(evukursdeltakelse, EvuKursDeltakelse)

    def list_vurderingskombinasjoner(
        self, expand: bool = False, limit: int = 10, **kwargs
    ) -> Iterator[Union[VurderingsKombinasjon, VurderingsKombinasjonId, dict]]:
        fs_params = FsParams()
        params = {"limit": limit}
        params.update(fs_params(kwargs))
        url = self.urls.list_vurderingskombinasjoner()
        kombinasjoner = self.get(url, params=params)
        for kombinasjon in self.depaginate(kombinasjoner):
            href = extract_href(kombinasjon)
            if expand:
                res = self.get(href)
                yield self.dict_or_model(res, VurderingsKombinasjon)
            else:
                yield self.str_or_model(href, VurderingsKombinasjonId)

    def get_vurderingskombinasjon(
        self, vurderingskombinasjon_id: str
    ) -> Optional[Union[VurderingsKombinasjon, dict]]:
        url = self.urls.get_vurderingskombinasjon(vurderingskombinasjon_id)
        try:
            vurderingskombinasjon = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(vurderingskombinasjon, VurderingsKombinasjon)

    def list_vurderingsenheter(
        self, expand: bool = False, limit: int = 10, **kwargs
    ) -> Iterator[Union[VurderingsEnhet, VurderingsEnhetId, dict]]:
        fs_params = FsParams(
            {
                "emne.institusjon": "vurderingskombinasjon.emne.institusjon",
                "emne.kode": "vurderingskombinasjon.emne.kode",
                "emne.versjon": "vurderingskombinasjon.emne.versjon",
            }
        )
        params = {"limit": limit}
        params.update(fs_params(kwargs))
        url = self.urls.list_vurderingsenheter()
        enheter = self.get(url, params=params)
        for enhet in self.depaginate(enheter):
            href = extract_href(enhet)
            if expand:
                res = self.get(href)
                yield self.dict_or_model(res, VurderingsEnhet)
            else:
                yield self.str_or_model(href, VurderingsEnhetId)

    def get_vurderingsenhet(
        self, vurderingsenhet_id: str
    ) -> Optional[Union[VurderingsEnhet, dict]]:
        url = self.urls.get_vurderingsenhet(vurderingsenhet_id)
        try:
            vurderingsenhet = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(vurderingsenhet, VurderingsEnhet)

    def get_vurderingstid(
        self, vurderingstid_id: str
    ) -> Optional[Union[VurderingsTid, dict]]:
        """ /vurderingstider/{id} """
        url = self.urls.get_vurderingstid(vurderingstid_id)
        try:
            vurderingstid = self.get(url)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                return None
            raise
        return self.dict_or_model(vurderingstid, VurderingsTid)

    def list_studentvurderinger(
        self, expand: bool = False, limit: int = 10, **kwargs
    ) -> Iterator[Union[StudentVurdering, StudentVurderingId, dict]]:
        fs_params = FsParams(
            {
                "person.id": "person.personlopenummer",
                "emne.institusjon": "vurderingsenhet.vurderingskombinasjon.emne.institusjon",
                "emne.kode": "vurderingsenhet.vurderingskombinasjon.emne.kode",
                "emne.versjon": "vurderingsenhet.vurderingskombinasjon.emne.versjon",
                "vurderingskombinasjon.kode": "vurderingsenhet.vurderingskombinasjon.kode",
                "vurderingstid.ar": "vurderingsenhet.vurderingstid.ar",
                "vurderingstid.kode": "vurderingsenhet.vurderingstid.kode",
            }
        )
        params = {"limit": limit}
        params.update(fs_params(kwargs))
        url = self.urls.list_studentvurderinger()
        vurderinger = self.get(url, params=params)
        for vurdering in self.depaginate(vurderinger):
            href = extract_href(vurdering)
            if expand:
                yield self.dict_or_model(self.get(href), StudentVurdering)
            else:
                yield self.str_or_model(href, StudentVurderingId)

    def get_studentvurdering(
        self, studentvurdering_id: str
    ) -> Optional[Union[StudentVurdering, dict]]:
        """ /studentvurderinger/{id} """
        url = self.urls.get_studentvurdering(studentvurdering_id)
        studentvurdering = self.get(url)
        return self.dict_or_model(studentvurdering, StudentVurdering)

    def get_vurderingsresultatstatuser(
        self, resultat_id: str
    ) -> Optional[Union[VurderingsResultatStatus, dict]]:
        """ /vurderingsresultatstatuser/{id} """
        url = self.urls.get_vurderingsresultatstatuser(resultat_id)
        status = self.get(url)
        return self.dict_or_model(status, VurderingsResultatStatus)

    def list_studieretter(
        self,
        expand: bool = False,
        limit: int = 100,
        params_for_expanded: dict = {},
        **kwargs,
    ) -> Iterator[Union[Studierett, StudierettId, dict]]:
        """ /studieretter """
        # Note that you can also filter on 'semester.ar' and 'semester.termin',
        # which refers to the ar/termin in the identifier, but not necessarily
        # the term which it applies to (!) Here be dragons.
        fs_params = FsParams(
            {
                "kull.semester.ar": "kull.ar",
                "kull.semester.termin": "kull.termin",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_studieretter()
        studieretter = self.get(url, params=params)
        for studierett_id in self.depaginate(studieretter):
            href = extract_href(studierett_id)
            if expand:
                studierett = self.get(href, params=params_for_expanded)
                yield self.dict_or_model(studierett, Studierett)
            else:
                yield self.str_or_model(href, StudierettId)

    def get_studierett(
        self, studierett_id: str, refId: bool = True
    ) -> Optional[Union[Studierett, dict]]:
        """ /studieretter/{id} """
        url = self.urls.get_studierett(studierett_id)
        params = {
            # Request that href structures in result be fully populated.
            # Of particular interest is the program option code
            # (studieretning.kode).
            "refId": refId,
        }
        studierett = self.get(url, params=params)
        return self.dict_or_model(studierett, Studierett)

    def list_semesterregistreringer(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[Semesterregistrering, SemesterregistreringId, dict]]:
        """ /semesterregistreringer """
        fs_params = FsParams(
            {
                "kull.semester.ar": "kull.ar",
                "kull.semester.termin": "kull.termin",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_semesterregistreringer()
        semesterregistreringer = self.get(url, params=params)
        for semesterregistrering_id in self.depaginate(semesterregistreringer):
            href = extract_href(semesterregistrering_id)
            if expand:
                semesterregistrering = self.get(href)
                yield self.dict_or_model(semesterregistrering, Semesterregistrering)
            else:
                yield self.str_or_model(href, SemesterregistreringId)

    def get_semesterregistrering(
        self, semesterregistrering_id: str
    ) -> Optional[Union[Semesterregistrering, dict]]:
        """ /semesterregistreringer/{id} """
        url = self.urls.get_semesterregistrering(semesterregistrering_id)
        semesterregistrering = self.get(url)
        return self.dict_or_model(semesterregistrering, Semesterregistrering)

    def list_studieprogrammer(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[Studieprogram, StudieprogramId, dict]]:
        """ /studieprogrammer """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_studieprogrammer()
        studieprogrammer = self.get(url, params=params)
        for studieprogram_id in self.depaginate(studieprogrammer):
            href = extract_href(studieprogram_id)
            if expand:
                studieprogram = self.get(href)
                yield self.dict_or_model(studieprogram, Studieprogram)
            else:
                yield self.str_or_model(href, StudieprogramId)

    def get_studieprogram(
        self, studieprogram_id: str
    ) -> Optional[Union[Studieprogram, dict]]:
        """ /studieprogrammer/{id} """
        url = self.urls.get_studieprogram(studieprogram_id)
        studieprogram = self.get(url)
        return self.dict_or_model(studieprogram, Studieprogram)

    def list_kull(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[Kull, KullId, dict]]:
        """ /kull """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_kull()
        all_kull = self.get(url, params=params)
        for kull_id in self.depaginate(all_kull):
            href = extract_href(kull_id)
            if expand:
                kull = self.get(href)
                yield self.dict_or_model(kull, Kull)
            else:
                yield self.str_or_model(href, KullId)

    def get_kull(self, kull_id: str) -> Optional[Union[Kull, dict]]:
        """ /kull/{id} """
        url = self.urls.get_kull(kull_id)
        kull = self.get(url)
        return self.dict_or_model(kull, Kull)

    def list_kullklasser(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[KullKlasse, KullKlasseId, dict]]:
        """ /kullklasser """
        fs_params = FsParams(
            {
                "studieprogram.kode": "kull.studieprogram.kode",
                "semester.ar": "kull.semester.ar",
                "semester.termin": "kull.semester.termin",
            }
        )
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_kullklasser()
        kullklasser = self.get(url, params=params)
        for kullklasse_id in self.depaginate(kullklasser):
            href = extract_href(kullklasse_id)
            if expand:
                kullklasse = self.get(href)
                yield self.dict_or_model(kullklasse, KullKlasse)
            else:
                yield self.str_or_model(href, KullKlasseId)

    def get_kullklasse(self, kullklasse_id: str) -> Optional[Union[KullKlasse, dict]]:
        """ /kullklasse/{id} """
        url = self.urls.get_kullklasse(kullklasse_id)
        kullklasse = self.get(url)
        return self.dict_or_model(kullklasse, KullKlasse)

    def list_kullklassestudenter(
        self, expand: bool = False, limit: int = 100, **kwargs
    ) -> Iterator[Union[KullKlasseStudentId, KullKlasseStudent, dict]]:
        """ /kullklassestudenter """
        fs_params = FsParams()
        params = {
            "limit": limit,
        }
        params.update(fs_params(kwargs))
        url = self.urls.list_kullklassestudenter()
        kullklassestudenter = self.get(url, params=params)
        for item in self.depaginate(kullklassestudenter):
            href = extract_href(item)
            if expand:
                kullklassestudent = self.get(href, params={"dbId": "true"})
                yield self.dict_or_model(kullklassestudent, KullKlasseStudent)
            else:
                yield self.str_or_model(href, KullKlasseStudentId)

    def get_kullklassestudent(self, kullklassestudent_id: str) -> Optional[Union[KullKlasse, dict]]:
        """ /kullklassestudenter/{id} """
        url = self.urls.get_kullklassestudent(kullklassestudent_id)
        params = {"dbId": "true"}
        kullklassestudent = self.get(url, params=params)
        return self.dict_or_model(kullklassestudent, KullKlasseStudent)

    def get_organisasjonsenhet(
        self,
        organisasjonsenhet_id: str,
        **kwargs,
    ) -> Optional[Union[Organisasjonsenhet, dict]]:
        """ /organisasjonsenheter/{id} """
        url = self.urls.get_organisasjonsenhet(organisasjonsenhet_id)
        return self.dict_or_model(self.get(url, params=kwargs), Organisasjonsenhet)

    def list_organisasjonsenheter(
        self,
        limit: int = 10,
        **kwargs,
    ) -> Iterator[Union[Organisasjonsenhet, dict]]:
        """ /organisasjonsenheter """
        params = {
            "limit": limit,
        }
        params.update(kwargs)
        url = self.urls.list_organisasjonsenheter()
        organisasjonsenheter = self.get(url, params=params)
        for organisasjonsenhet in self.depaginate(organisasjonsenheter):
            yield self.dict_or_model(organisasjonsenhet, Organisasjonsenhet)


def get_client(config_dict):
    """
    Get a FsClient from configuration.
    """
    return FsClient(
        config_dict["url"],
        headers=config_dict.get("headers"),
        rewrite_url=config_dict.get("rewrite_url"),
        use_models=config_dict.get("use_models", DEFAULT_USE_MODELS),
    )
