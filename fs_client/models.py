"""
Models for FS identifiers and objects.

An FS identifier (`fs_id`) is a string. It should contain a sequence of
comma-separated fields.  E.g. "123,FOO101,1".

An FS object is a regular JSON object.
"""
import datetime
import json
from enum import Enum
from typing import Any, Generic, List, Optional, Type, TypeVar, Union

import pydantic
from pydantic import AnyUrl

from .utils import extract_id, split_id

TFsIdentifier = TypeVar("TFsIdentifier", bound="FsIdentifier")
TFsObject = TypeVar("TFsObject", bound="FsObject")


class NonEmptyStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise ValueError(f"str expected, not {type(v)}")
        if not v:
            raise ValueError("non-empty string expected")
        return v


class FsObject(Generic[TFsObject], pydantic.BaseModel):
    """
    Abstract FS object.
    """

    @classmethod
    def from_dict(cls, data):
        return cls(**data)

    @classmethod
    def from_json(cls, json_data):
        data = json.loads(json_data)
        return cls.from_dict(data)


class FsIdentifier(Generic[TFsIdentifier], pydantic.BaseModel):
    """
    Abstract FS identifier.
    """

    _fetch_method: str

    @classmethod
    def from_fs_id(
        cls: Type[TFsIdentifier], fs_id: Union[List[str], str]
    ) -> TFsIdentifier:
        """Initialize from a raw FS identifier string."""
        if isinstance(fs_id, str):
            fs_id = split_id(fs_id)
        return cls(**dict(zip(cls.__fields__.keys(), fs_id)))

    @property
    def fs_id(self) -> str:
        """The raw FS identifier string."""
        return ",".join(self.dict().values())

    def fetch(self, client):
        """
        Look up object data using a given client.

        :type client: fs_client.client.FsClient
        """
        fetch = getattr(client, self._fetch_method)
        return fetch(self.fs_id)

    def __str__(self) -> str:
        return "{cls.__name__}:{obj.fs_id}".format(cls=type(self), obj=self)

    def __hash__(self) -> int:
        return hash(self.fs_id)

    def as_filter(self) -> dict:
        return self.dict()


class SemesterRef(FsObject):
    href: AnyUrl
    type: Optional[str]
    ar: Optional[int]
    termin: Optional[str]


class EmneId(FsIdentifier):
    """
    Subject FS identifier (emne).

    Fields:

    - emne_institusjon
    - emne_kode
    - emne_versjon

    `fs_id` example: "123,FOO101,1"
    """

    emne_institusjon: NonEmptyStr
    emne_kode: NonEmptyStr
    emne_versjon: NonEmptyStr

    _fetch_method = "get_emne"


class UndervisningId(EmneId):
    """
    Lesson FS identifier (undervisningsenhet) .

    Fields from :py:class:`EmneId`, and adds:

    - semester_ar
    - semester_termin
    - undervisningsenhet_terminnummer

    `fs_id` example: "123,FOO101,1,1999,VÅR,1"
    """

    semester_ar: NonEmptyStr
    semester_termin: NonEmptyStr
    undervisningsenhet_terminnummer: NonEmptyStr

    _fetch_method = "get_undervisning"

    @property
    def emne(self) -> EmneId:
        return EmneId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                }
            )
        )


class StudentUndervisningId(UndervisningId):
    """
    Student lesson participation FS identifier.

    Links a student object to a :py:class:`UndervisningId`.  Fields from
    :py:class:`UndervisningId`, and adds:

    - person_id

    `fs_id` example: "123,FOO101,1,1999,VÅR,1,12345"
    """

    person_id: NonEmptyStr

    _fetch_method = "get_studentundervisning"

    @property
    def undervisning(self) -> UndervisningId:
        return UndervisningId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                    "semester_ar",
                    "semester_termin",
                    "undervisningsenhet_terminnummer",
                }
            )
        )


class AktivitetId(UndervisningId):
    """
    Activity FS identifier (undervisningsaktivitet).

    Links an activity to a :py:class:`UndervisningId`.  Fields from
    :py:class`UndervisningId`, and adds:

    - undervisningsaktivitet_aktivitet

    `fs_id` examples:

    - "123,FOO101,1,1999,VÅR,1,1"
    - "123,FOO101,1,1999,VÅR,1,2-1"
    """

    undervisningsaktivitet_aktivitet: NonEmptyStr

    _fetch_method = "get_undervisningsaktivitet"

    @property
    def undervisning(self) -> UndervisningId:
        return UndervisningId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                    "semester_ar",
                    "semester_termin",
                    "undervisningsenhet_terminnummer",
                    "undervisningsaktivitet_aktivitet",
                }
            )
        )


class StudentAktivitetId(AktivitetId):
    """
    Student activity participation FS identifier.

    :py:class:`StudentAktivitetId` links a student object to an
    :py:class:`AktivitetId`.  Fields from :py:class:`AktivitetId`, and adds:

    - parti
    - form
    - disiplin
    - person_id

    `fs_id` example: "123,FOO101,1,1999,VÅR,1,2-1,1,ØVE,TEORI,12345"
    """

    parti: NonEmptyStr
    form: NonEmptyStr
    disiplin: NonEmptyStr
    person_id: NonEmptyStr

    _fetch_method = "get_studentundervisningsaktivitet"

    @property
    def aktivitet(self) -> AktivitetId:
        return AktivitetId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                    "semester_ar",
                    "semester_termin",
                    "undervisningsenhet_terminnummer",
                    "undervisningsaktivitet_aktivitet",
                }
            )
        )


class EvuKursId(FsIdentifier):
    """
    Continuing and further education course FS identifier (EVU-kurs).

    Fields:

    - kode
    - tidsangivelse

    `fs_id` example: "FOO1234,2018H"
    """

    kode: NonEmptyStr
    tidsangivelse: NonEmptyStr

    _fetch_method = "get_evukurs"

    def as_filter(self):
        return {
            "evuKurs_kode": self.kode,
            "evuKurs_tidsangivelse": self.tidsangivelse,
        }


class EvuKursDeltakelseId(FsIdentifier):
    """
    Continuing and further education course participation FS identifier
    (EVU-kursdeltakelse).

    Links a participation to an :py:class:`EvuKursId`.

    Fields:

    - deltaker_nummer

    `fs_id` example: "5678,FOO1234,2018H"
    """

    deltaker_nummer: NonEmptyStr
    kode: NonEmptyStr
    tidsangivelse: NonEmptyStr

    _fetch_method = "get_evukursdeltakelse"

    @property
    def evukurs(self) -> EvuKursId:
        return EvuKursId(
            **self.dict(
                include={
                    "kode",
                    "tidsangivelse",
                }
            )
        )


class StudieprogramId(FsIdentifier):
    """
    Study programme FS identifier

    Fields:
    - studieprogram_kode

    `fs_id` example: "INFMASEF"
    """

    studieprogram_kode: NonEmptyStr

    _fetch_method = "get_studieprogram"


class KullId(StudieprogramId):
    """
    Cohort FS identifier

    Fields from :py:class:`'StudieprogramId`, and adds:

    - kull_semester_ar
    - kull_semester_termin

    `fs_id` example: "INFMASEF,2018,HØST"
    """

    kull_semester_ar: NonEmptyStr
    kull_semester_termin: NonEmptyStr

    _fetch_method = "get_kull"

    @property
    def studieprogram(self) -> StudieprogramId:
        return StudieprogramId(
            **self.dict(
                include={
                    "studieprogram_kode",
                }
            )
        )


class KullKlasseId(KullId):
    """
    Class FS identifier

    Fields from :py:class:`'KullId`, and adds:

    - kullklasse_kode

    `fs_id` example: "INFMASEF,2018,HØST"
    """

    kullklasse_kode: NonEmptyStr

    _fetch_method = "get_kullklasse"

    @property
    def kull(self) -> KullId:
        return KullId(
            **self.dict(
                include={
                    "studieprogram_kode",
                    "kull_semester_ar",
                    "kull_semester_termin",
                }
            )
        )


class StudierettId(KullId):
    """
    Admission FS identifier

    Fields from :py:class:`'KullId`, and adds:

    - person_personlopenummer

    `fs_id` example: "INFMASEF,2018,HØST,1234"
    """

    person_personlopenummer: NonEmptyStr

    _fetch_method = "get_studierett"

    @property
    def kull(self) -> KullId:
        return KullId(
            **self.dict(
                include={
                    "studieprogram_kode",
                    "kull_semester_ar",
                    "kull_semester_termin",
                }
            )
        )


class KullKlasseStudentId(FsIdentifier):
    """
    Class registration FS Identifier

    Fields:
    - studierett_studieprogram_kode
    - studierett_semester_ar
    - studierett_semester_termin
    - studierett_person_personlopenummer
    - kullklasse_kull_studieprogram_kode
    - kullklasse_kull_semester_ar
    - kullklasse_kull_semester_termin
    - kullklasse_kode

    `fs_id` example: "HFB-ARKO,2020,HØST,1001234,HFB-ARKO,2020,HØST,ARK"
    """

    studierett_studieprogram_kode: NonEmptyStr
    studierett_semester_ar: NonEmptyStr
    studierett_semester_termin: NonEmptyStr
    studierett_person_personlopenummer: NonEmptyStr
    kullklasse_kull_studieprogram_kode: NonEmptyStr
    kullklasse_kull_semester_ar: NonEmptyStr
    kullklasse_kull_semester_termin: NonEmptyStr
    kullklasse_kode: NonEmptyStr

    _fetch_method = "get_kullklassestudent"

    @property
    def studierett(self) -> StudierettId:
        return StudierettId(
            studieprogram_kode=self.studierett_studieprogram_kode,
            kull_semester_ar=self.studierett_semester_ar,
            kull_semester_termin=self.studierett_semester_termin,
            person_personlopenummer=self.studierett_person_personlopenummer,
        )

    @property
    def kullklasse(self) -> KullKlasseId:
        return KullKlasseId(
            studieprogram_kode=self.kullklasse_kull_studieprogram_kode,
            kull_semester_ar=self.kullklasse_kull_semester_ar,
            kull_semester_termin=self.kullklasse_kull_semester_termin,
            kullklasse_kode=self.kullklasse_kode,
        )


class SemesterregistreringId(FsIdentifier):
    """
    Semester registration FS identifier

    Fields:
    - person_personlopenummer
    - semester_ar
    - semester_termin

    `fs_id` example: "1018900,2018,HØST"
    """

    person_personlopenummer: NonEmptyStr
    semester_ar: NonEmptyStr
    semester_termin: NonEmptyStr

    _fetch_method = "get_semesterregistrering"


class RolleId(FsIdentifier):
    """
    Role FS identifier

    Fields:
    - kode

    `fs_id` example: "SENSOR"
    """

    kode: NonEmptyStr

    _fetch_method = "get_rolle"


class PersonId(FsIdentifier):
    """
    Person FS identifier

    Fields:
    - personlopenummer

    `fs_id` example: "12345"
    """

    personlopenummer: NonEmptyStr

    _fetch_method = "get_person"


class PersonRolleId(FsIdentifier):
    """
    Person role FS identifier

    Fields:
    - person_id
    - rollenummer

    `fs_id` example: "123456,1"
    """

    person_id: NonEmptyStr
    rollenummer: NonEmptyStr

    _fetch_method = "get_personrolle"


class DeltakerId(FsIdentifier):
    """
    Participant FS identifier

    Fields:
    - nummer

    `fs_id` example: "12345"
    """

    nummer: NonEmptyStr

    _fetch_method = "get_deltaker"


class Navn(FsObject):
    lang: str
    value: str


class Periode(FsObject):
    type: Optional[str]
    fraDato: Optional[datetime.date]
    tilDato: Optional[datetime.date]


class StudienivaRef(FsObject):
    href: AnyUrl


class TilbudstatusRef(FsObject):
    href: AnyUrl
    kode: Optional[str]


class OrganisasjonsenhetId(FsIdentifier):
    """
    FS organizational unit identifier

    Fields:
    - institusjon
    - fakultet
    - institutt
    - gruppe

    `fs_id` example: "194,67,80,0"
    """

    institusjon: NonEmptyStr
    fakultet: NonEmptyStr
    institutt: NonEmptyStr
    gruppe: NonEmptyStr

    _fetch_method = "get_organisasjonsenhet"

    @property
    def institusjon_ref(self):
        return OrganisasjonsenhetId(
            institusjon=self.institusjon, fakultet="0", institutt="0", gruppe="0"
        )

    @property
    def fakultet_ref(self):
        return OrganisasjonsenhetId(
            institusjon=self.institusjon,
            fakultet=self.fakultet,
            institutt="0",
            gruppe="0",
        )

    @property
    def institutt_ref(self):
        return OrganisasjonsenhetId(
            institusjon=self.institusjon,
            fakultet=self.fakultet,
            institutt=self.institutt,
            gruppe="0",
        )


class OrganisasjonsenhetRef(FsObject):
    href: AnyUrl
    type: str

    @property
    def ref(self) -> OrganisasjonsenhetId:
        return OrganisasjonsenhetId.from_fs_id(extract_id(self.href))


class EmneRef(FsObject):
    href: AnyUrl
    type: Optional[str]
    institusjon: Optional[int]
    kode: Optional[str]
    versjon: Optional[str]

    @property
    def ref(self) -> EmneId:
        return EmneId.from_fs_id(extract_id(self.href))


class UndervisningRef(FsObject):
    href: AnyUrl
    type: Optional[str]
    emne: EmneRef
    semester: SemesterRef
    terminnummer: int

    @property
    def ref(self) -> UndervisningId:
        return UndervisningId.from_fs_id(extract_id(self.href))


class AktivitetRef(FsObject):
    href: AnyUrl
    undervisning: UndervisningRef
    aktivitet: str


class AktivitetEierRef(FsObject):
    href: AnyUrl
    type: str

    @property
    def ref(self) -> AktivitetId:
        return AktivitetId.from_fs_id(extract_id(self.href))


class RolleRef(FsObject):
    href: AnyUrl

    @property
    def ref(self) -> RolleId:
        return RolleId.from_fs_id(extract_id(self.href))


class PersonRef(FsObject):
    href: AnyUrl
    personlopenummer: int


class LandRef(FsObject):
    href: AnyUrl
    type: Optional[str]
    nummer: Optional[int]


class EvuKursRef(FsObject):
    href: AnyUrl
    kode: str
    tidsangivelse: str

    @property
    def ref(self) -> EvuKursId:
        return EvuKursId.from_fs_id(extract_id(self.href))


class DeltakerRef(FsObject):
    href: AnyUrl
    nummer: int

    @property
    def ref(self) -> DeltakerId:
        return DeltakerId.from_fs_id(extract_id(self.href))


class StudieretningRef(FsObject):
    href: AnyUrl
    kode: Optional[str]
    type: Optional[str]


class StudieprogramRef(FsObject):
    href: AnyUrl
    kode: NonEmptyStr


class StudierettRef(FsObject):
    href: Optional[AnyUrl]
    person: PersonRef
    studieprogram: StudieprogramRef
    termin: SemesterRef


class KullRef(FsObject):
    href: AnyUrl
    semester: SemesterRef
    studieprogram: StudieprogramRef


class KullKlasseRef(FsObject):
    href: AnyUrl
    kull: KullRef
    # kode is currently missing. bug.
    kode: Optional[NonEmptyStr]
    # kode: NonEmptyStr


class KullKlasseStudentRef(FsObject):
    href: Optional[AnyUrl]
    studierett: StudierettRef
    kullklasse: KullKlasseRef


class FsBasicRef(FsObject):
    """Abstract FS reference object, containing only an 'href' field."""

    _ref_type: Type[FsIdentifier]
    href: AnyUrl

    @property
    def ref(self) -> FsIdentifier:
        return self._ref_type.from_fs_id(extract_id(self.href))


class PersonBasicRef(FsBasicRef):
    _ref_type = PersonId


class EmneBasicRef(FsBasicRef):
    _ref_type = EmneId


class UndervisningBasicRef(FsBasicRef):
    _ref_type = UndervisningId


class AktivitetBasicRef(FsBasicRef):
    _ref_type = AktivitetId


class EvuKursBasicRef(FsBasicRef):
    _ref_type = EvuKursId


class StudieprogramBasicRef(FsBasicRef):
    _ref_type = StudieprogramId


class KullBasicRef(FsBasicRef):
    _ref_type = KullId


class KullKlasseBasicRef(FsBasicRef):
    _ref_type = KullKlasseId


class Kvalifikasjonsgrunnlag(FsObject):
    href: AnyUrl
    type: str
    kode: Optional[str]


class Telefon(FsObject):
    type: Optional[str]
    landnummer: Optional[str]
    nummer: Optional[str]
    merknad: Optional[str]


class Adresse(FsObject):
    type: Optional[str]
    gate: Optional[str]
    sted: Optional[str]
    postnummer: Optional[str]
    land: Optional[str]
    co: Optional[str]


class Epost(FsObject):
    adresse: str
    type: Optional[str]


class Fylke(FsObject):
    type: Optional[str]
    nummer: int
    navn: str
    region: Optional[int]
    felles: bool


class Kommune(FsObject):
    type: str
    nummer: int
    navn: str
    fylke: Fylke
    felles: bool


class SprakRef(FsObject):
    href: Optional[AnyUrl]
    type: str


class Student(FsObject):
    studentnummer: int
    adresser: Optional[List[Adresse]]
    lantakerId: Optional[str]


class Stillingstittel(FsObject):
    sprak: Optional[SprakRef]
    value: str


class FagpersonStatus(FsObject):
    kode: str
    navn: str
    felles: bool


class Fagperson(FsObject):
    aktiv: bool
    ekstern: Optional[bool]
    permisjon: Optional[bool]
    organisasjonsenhet: Optional[OrganisasjonsenhetRef]
    adresser: Optional[List[Adresse]]
    stillingstitler: Optional[List[Stillingstittel]]
    tittelprefix: Optional[str]
    fagpersonstatus: Optional[FagpersonStatus]
    merknad: Optional[str]


class Person(FsObject):
    href: AnyUrl
    personlopenummer: int
    fodselsdato: int
    personnummer: int
    brukernavn: Optional[str]
    fornavn: str
    etternavn: str
    forkortetNavn: str
    kjonn: str
    epost: Optional[Epost]
    eposter: Optional[List[Epost]]
    telefoner: Optional[List[Telefon]]
    adresser: Optional[List[Adresse]]
    kvalifikasjonsgrunnlag: Optional[Kvalifikasjonsgrunnlag]
    organisasjonsenheter: Optional[List[OrganisasjonsenhetRef]]
    kommuner: Optional[List[Kommune]]
    land: Optional[List[LandRef]]
    semestre: Optional[List[SemesterRef]]
    ansattnummer: Optional[str]
    dod: Optional[bool]
    reservertNettPublisering: Optional[bool]
    reservertLms: Optional[bool]
    datoFodt: datetime.date
    sprak: Optional[List[SprakRef]]
    student: Optional[Student]
    fagperson: Optional[Fagperson]


class PersonRolle(FsObject):
    href: AnyUrl
    person: PersonRef
    rollenummer: int
    rolle: RolleRef
    periode: Optional[Periode]
    emne: Optional[EmneBasicRef]
    undervisning: Optional[UndervisningBasicRef]
    undervisningsaktivitet: Optional[AktivitetBasicRef]
    # organisasjonsenhet: Optional[OrganisasjonsenhetBasicRef]
    evuKurs: Optional[EvuKursBasicRef]
    studieprogram: Optional[StudieprogramBasicRef]
    kull: Optional[KullBasicRef]
    kullklasse: Optional[KullKlasseBasicRef]

    @property
    def ref(self) -> PersonRolleId:
        return PersonRolleId.from_fs_id(extract_id(self.href))


class Rolle(FsObject):
    href: AnyUrl
    kode: str
    navn: List[Navn]
    publiseringsRollenavn: Optional[str]
    langtLmsRollenavn: Optional[str]
    publisering: bool
    undervisning: bool
    aktiv: bool
    fagpersonWebTilgang: bool
    felles: bool
    imsRolle: int
    merknad: Optional[str]


class Deltaker(FsObject):
    href: AnyUrl
    nummer: int
    person: PersonBasicRef
    fornavn: str
    etternavn: str
    kjonn: str
    telefoner: Optional[List[Telefon]]
    adresser: Optional[List[Adresse]]
    eposter: Optional[List[Epost]]
    land: Optional[LandRef]
    sprak: Optional[List[SprakRef]]
    # maalform: str
    # fodselsdato is listed in the docs, but MIA since FS-API 1.6.0
    # fodselsdato: int


class Lms(FsObject):
    eksport: bool
    romMal: Optional[str]
    lopenummer: Optional[int]


class Emne(FsObject):
    href: AnyUrl
    institusjon: int
    kode: str
    versjon: str
    navn: List[Navn]
    studieniva: StudienivaRef
    organisasjonsenheter: List[OrganisasjonsenhetRef]
    hovedoppgave: bool

    @property
    def ref(self) -> EmneId:
        return EmneId.from_fs_id(extract_id(self.href))


class Undervisning(FsObject):
    href: AnyUrl
    emne: EmneRef
    semester: SemesterRef
    terminnummer: int
    lukket: bool
    lms: Lms
    antallTilbud: Optional[int]
    perioder: Optional[List[Periode]]

    @property
    def ref(self) -> UndervisningId:
        return UndervisningId.from_fs_id(extract_id(self.href))


class Aktivitet(FsObject):
    """Undervisningsaktivitet"""

    href: AnyUrl
    undervisning: UndervisningRef
    eier: AktivitetEierRef
    aktivitet: str
    parti: Optional[int]
    form: Optional[str]
    disiplin: Optional[str]
    navn: List[Navn]
    nummer: int
    kanOnskes: Optional[bool]
    apentHvisHoySokning: bool
    publiseres: bool
    eksporterTimeplan: bool
    lms: Lms
    kapasitet: Optional[int]
    lukket: bool
    perioder: Optional[List[Periode]]
    pameldingSlutt: Optional[datetime.datetime]
    publiserPlass: Optional[bool]
    avtale: Optional[bool]
    antallOnsket: Optional[int]
    romtypeOnsket: Optional[str]
    campus: Optional[str]
    fellesaktivitet: Optional[str]

    @property
    def ref(self) -> AktivitetId:
        return AktivitetId.from_fs_id(extract_id(self.href))


class Beskrivelse(FsObject):
    type: str
    beskrivelse: str


class StudierettStatus(FsObject):
    aktiv: bool
    beholdVedOvergang: bool
    felles: bool
    kode: str
    navn: List[Navn]


class EvuKurs(FsObject):
    href: AnyUrl
    kode: str
    tidsangivelse: str
    navn: List[Navn]
    organisasjonsenheter: List[OrganisasjonsenhetRef]
    perioder: Optional[List[Periode]]
    kurstype: Optional[str]
    lms: Lms
    aktiv: bool
    fjernundervisning: bool
    kanTilbys: bool
    skalAvholdes: bool
    desentralUndervisning: bool
    nettbasertUndervisning: bool
    undervisningVedInstitusjon: bool
    kontoTildeling: bool
    automatiskTilbud: bool
    automatiskJaSvar: bool
    fritakSemesteravgift: bool
    automatiskEksamen: bool
    forbeholdtForetak: bool
    ccKvitteringsEpost: bool
    kravOmFodselsnummer: Optional[bool]
    url: Optional[str]
    epost: Optional[Epost]
    beskrivelse: Optional[List[Beskrivelse]]
    studierettStatus: Optional[StudierettStatus]
    soknadsfrist: Optional[datetime.datetime]
    pameldingsfrist: Optional[datetime.datetime]
    svarfrist: Optional[datetime.datetime]
    trekkfrist: Optional[datetime.datetime]
    kull: Optional[KullBasicRef]
    minimumAntallDeltakere: Optional[int]
    maksimumAntallDeltakere: Optional[int]
    totaltAntallDeltakere: Optional[int]
    gjennomfortAntallDeltakere: Optional[int]
    studieprogram: Optional[StudieprogramBasicRef]
    totaltAntallKvinner: Optional[int]
    gjennomfortAntallKvinner: Optional[int]
    kostnader: Optional[float]
    budsjettGodkjent: Optional[datetime.datetime]
    budsjettutgifter: Optional[float]
    regnskapsinntekter: Optional[float]
    regnskapsutgifter: Optional[float]
    utarbeidetBudsjett: Optional[datetime.datetime]
    antallArsverk: Optional[float]


class EvuKursSvarstatus(FsObject):
    kode: str
    navn: List[Navn]
    felles: bool
    nasjonalOpptaksmodell: bool


class EvuKursDeltakelse(FsObject):
    href: AnyUrl
    deltaker: DeltakerRef
    evuKurs: EvuKursRef
    vurdering: Optional[bool]
    tilbudStatus: Optional[TilbudstatusRef]
    svarstatus: Optional[EvuKursSvarstatus]


class StudentUndervisning(FsObject):
    href: AnyUrl
    person: PersonRef
    undervisning: UndervisningRef
    publiserOpptak: bool
    opptatt: bool
    kvalifisert: Optional[bool]
    fremmote: Optional[bool]
    betingetKvalifisert: Optional[bool]
    svarPaTilbud: Optional[bool]
    tilbudstatus: Optional[TilbudstatusRef]
    pameldingsrekkefolge: Optional[int]
    ventelistenummer: Optional[int]
    loddtrekningsnummer: Optional[int]

    @property
    def ref(self) -> StudentUndervisningId:
        return StudentUndervisningId.from_fs_id(extract_id(self.href))


class StudentAktivitet(FsObject):
    href: AnyUrl
    person: PersonRef
    undervisningsaktivitet: AktivitetRef
    parti: int
    form: str
    disiplin: str
    spesialtilbud: bool
    prioritet: Optional[int]
    tildelt: Optional[bool]
    laSta: Optional[bool]
    sperrSletting: Optional[bool]

    @property
    def ref(self) -> StudentAktivitetId:
        return StudentAktivitetId.from_fs_id(extract_id(self.href))


class VurderingsResultatStatusId(FsIdentifier):
    kode: NonEmptyStr

    _fetch_method = "get_vurderingsresultatstatuser"


class VurderingsResultatStatus(FsObject):
    href: AnyUrl
    kode: NonEmptyStr
    navn: List[Navn]
    gjentak: bool
    prioritet: Optional[int]
    felles: bool

    @property
    def ref(self) -> VurderingsResultatStatusId:
        return VurderingsResultatStatusId.from_fs_id(extract_id(self.href))


class VurderingsResultatStatusBasicRef(FsBasicRef):
    _ref_type = VurderingsResultatStatusId


class VurderingsTidId(FsIdentifier):
    ar: NonEmptyStr
    kode: NonEmptyStr

    _fetch_method = "get_vurderingstid"


class VurderingsTidBasicRef(FsBasicRef):
    _ref_type = VurderingsTidId


class VurderingsTidRef(VurderingsTidBasicRef):
    ar: int
    kode: NonEmptyStr


class ReellVurderingsTidRef(VurderingsTidBasicRef):
    type: NonEmptyStr


class VurderingsTid(VurderingsTidRef):
    semester: SemesterRef
    aktiv: bool
    eksternEksamen: bool
    rekkefolgenummer: int


class VurderingsStatus(FsObject):
    kode: NonEmptyStr
    navn: List[Navn]
    aktiv: bool
    studentweb: bool
    alle: bool
    mottsisteeks: bool
    stryksisteres: bool
    stryk: bool
    mott: bool
    avbrudd: bool
    avbruddsisteres: bool
    legesisteres: bool
    felles: bool


class DataSystem(FsObject):
    datasystemNavn: NonEmptyStr
    datasystemKode: NonEmptyStr
    kode: NonEmptyStr
    datasystemkode: NonEmptyStr
    navn: NonEmptyStr
    opptak: bool
    eksamen: bool
    sensur: bool
    beskrivelse: Optional[NonEmptyStr]
    felles: bool


class TidsEnhet(FsObject):
    kode: NonEmptyStr
    navn: List[Navn]
    aktiv: bool
    rekkefolgenummer: int


class Varighet(FsObject):
    verdi: float
    tidsenhet: TidsEnhet


class VurderingsKombinasjonId(EmneId):
    vurderingskombinasjon_kode: NonEmptyStr

    _fetch_method = "get_vurderingskombinasjon"


class VurderingsKombinasjonRef(FsObject):
    href: AnyUrl
    emne: EmneRef
    kode: NonEmptyStr
    versjon: Optional[NonEmptyStr]

    @property
    def ref(self) -> VurderingsKombinasjonId:
        return VurderingsKombinasjonId.from_fs_id(extract_id(self.href))


class VurderingsKombinasjon(FsObject):
    # All part of the id:
    href: AnyUrl
    emne: EmneRef
    kode: NonEmptyStr

    navn: List[Navn]
    lms: Lms
    datasystemer: Optional[List[DataSystem]]
    minutterOppmoteFoer: Optional[int]
    timerEkstraEksamenstidSpesialtilpasning: Optional[int]
    minutterEkstraEksamenstidSpesialtilpasning: Optional[int]
    aktiv: Optional[bool]
    eksamensavvikling: bool
    individueltTidspunkt: Optional[bool]
    klageViaStudentweb: Optional[bool]
    begrunnelseViaStudentweb: Optional[bool]
    niva: int
    sorteringsrekkefolge: NonEmptyStr
    merknad: Optional[NonEmptyStr]

    # TODO:
    #   vurderingskombinasjonstype:
    #   vurderingsordning:
    #   vurderingsform:
    #   campus:
    #   karaktererregel:
    #   ekvivalenteVurderingskombinasjoner:

    @property
    def ref(self) -> VurderingsKombinasjonId:
        return VurderingsKombinasjonId.from_fs_id(extract_id(self.href))


class VurderingsEnhetId(VurderingsKombinasjonId):
    vurderingstid_ar: NonEmptyStr
    vurderingstid_kode: NonEmptyStr

    _fetch_method = "get_vurderingsenhet"

    @property
    def emne(self) -> EmneId:
        return EmneId(
            **self.dict(include={"emne_institusjon", "emne_kode", "emne_versjon"})
        )

    @property
    def vurderingskombinasjon(self) -> VurderingsKombinasjonId:
        return VurderingsKombinasjonId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                    "vurderingskombinasjon_kode",
                }
            )
        )


class VurderingsEnhet(FsObject):
    # All part of the id:
    href: AnyUrl
    vurderingskombinasjon: VurderingsKombinasjonRef
    vurderingstid: VurderingsTidRef

    reellVurderingstid: ReellVurderingsTidRef
    vurderingsstatus: VurderingsStatus
    varighet: Optional[Varighet]
    publiser: bool
    publiserEksamensdato: Optional[bool]
    sensurutleggFraMelding: Optional[bool]
    publiserKandidatTimeplan: Optional[bool]
    antallKandidater: Optional[int]
    datasystemer: Optional[List[DataSystem]]
    perioder: Optional[List[Periode]]
    forslagEksamensdato: Optional[datetime.date]
    eksamensdato: Optional[datetime.date]
    eksamensstartklokkeslett: Optional[datetime.time]
    innleveringsfrist: Optional[datetime.date]
    innleveringsklokkeslett: Optional[datetime.time]
    uttaksdato: Optional[datetime.date]
    uttaksklokkeslett: Optional[datetime.time]
    minutterOppmoteFoer: Optional[int]
    merknad: Optional[NonEmptyStr]

    # TODO: Are these datetime or date?
    #   trekkfrist: datetime.date
    #   sensurfrist: Optional[datetime.datetime]
    #   kunngjoringsfrist: Optional[datetime.datetime]
    #   klagefrist: Optional[datetime.datetime]
    #   oppdatertTittelfrist: Optional[datetime.datetime]

    @property
    def ref(self) -> VurderingsEnhetId:
        return VurderingsEnhetId.from_fs_id(extract_id(self.href))


class StudentVurderingId(FsIdentifier):
    person_personlopenummer: NonEmptyStr
    emne_institusjon: NonEmptyStr
    emne_kode: NonEmptyStr
    emne_versjon: NonEmptyStr
    vurderingskombinasjon_kode: NonEmptyStr
    vurderingstid_ar: NonEmptyStr
    vurderingstid_kode: NonEmptyStr

    _fetch_method = "get_studentvurdering"

    @property
    def vurderingsenhet(self) -> VurderingsEnhetId:
        return VurderingsEnhetId(
            **self.dict(
                include={
                    "emne_institusjon",
                    "emne_kode",
                    "emne_versjon",
                    "vurderingskombinasjon_kode",
                    "vurderingstid_ar",
                    "vurderingstid_kode",
                }
            )
        )


class StudentVurdering(FsObject):
    href: AnyUrl
    erKandidat: Optional[bool]
    protokoll: Optional[bool]
    gyldig: Optional[bool]
    vurderingsresultatstatus: Optional[VurderingsResultatStatusBasicRef]
    antallBestattNgang: Optional[int]

    @property
    def ref(self) -> StudentVurderingId:
        return StudentVurderingId.from_fs_id(extract_id(self.href))


class Studieprogram(FsObject):
    href: AnyUrl
    # id: StudieprogramRef
    navn: List[Navn]
    utgatt: bool
    studieniva: StudienivaRef
    organisasjonsenhet: OrganisasjonsenhetRef

    @property
    def ref(self) -> StudieprogramId:
        return StudieprogramId.from_fs_id(extract_id(self.href))


class Kull(FsObject):
    href: AnyUrl
    # id: KullRef
    navn: List[Navn]
    aktiv: bool
    lms: Lms

    @property
    def ref(self) -> KullId:
        return KullId.from_fs_id(extract_id(self.href))


class KullKlasse(FsObject):
    href: AnyUrl
    # id: KullKlasseRef
    navn: str
    aktiv: bool
    lms: Lms
    studieretning: Optional[StudieretningRef]
    organisasjonsenhet: Optional[OrganisasjonsenhetRef]
    # campus: CampusRef

    @property
    def ref(self) -> KullKlasseId:
        return KullKlasseId.from_fs_id(extract_id(self.href))


class KullKlasseStudent(FsObject):
    id: KullKlasseStudentRef
    aktiv: bool
    merknad: Optional[str]


class StudentStatus(FsObject):
    kode: str
    navn: List[Navn]  # TODO: with SprakRef
    sperrUtdanningsplan: bool
    aktivStudent: bool
    visUtdanningsplan: bool
    felles: bool


class BegrensetStudierett(FsObject):
    kode: str
    navn: str
    felles: bool


class Studierett(FsObject):
    href: AnyUrl
    person: PersonRef
    studieprogram: StudieprogramRef
    semester: SemesterRef
    # studieprogram, semester and person makes the ID
    termin: SemesterRef
    periode: Periode
    privatist: bool
    studentstatus: StudentStatus
    studierettstatus: StudierettStatus
    studieniva: StudienivaRef
    begrensetStudierett: Optional[BegrensetStudierett]
    studieretning: Optional[StudieretningRef]


class Studiemaal(FsObject):
    nummer: int
    navn: str
    kortnavn: str
    organisasjonsenhet: OrganisasjonsenhetRef
    aktiv: bool
    felles: bool


class Studentkort(FsObject):
    nummer: str
    kode: str
    periode: Periode
    organisasjonsenhet: Optional[OrganisasjonsenhetRef]


class Studieopplegg(FsObject):
    kode: str
    navn: str
    felles: bool


class Registreringsform(FsObject):
    kode: str
    navn: str
    aktiv: bool
    semesterkvittering: bool
    felles: bool


class Betalingsform(FsObject):
    kode: str
    navn: str
    aktiv: bool
    semesterkvittering: bool
    felles: bool


class Studentkategori(FsObject):
    kode: str
    navn: str
    felles: bool


class Undervisningstilhorighet(FsObject):
    kode: int
    navn: str
    felles: bool


class NormalprogresjonStatus(FsObject):
    kode: int
    navn: str
    felles: bool


class Semesterregistrering(FsObject):
    href: AnyUrl
    person: PersonRef
    semester: SemesterRef
    # person and semester makes the ID
    betalingOk: bool
    registreringOk: bool
    studieprogram: Optional[StudieprogramBasicRef]
    kull: Optional[KullBasicRef]
    kullklasse: Optional[KullKlasseBasicRef]
    organisasjonsenheter: Optional[List[OrganisasjonsenhetRef]]
    # campus: CampusRef: href, type, kode
    # fag: FagRef: href, type, kode
    registreringsform: Optional[Registreringsform]
    betalingsform: Optional[Betalingsform]
    studentkategori: Optional[Studentkategori]
    transaksjonsIdSemesterkort: Optional[int]
    kvitteringSkrevet: Optional[bool]
    ugyldig: Optional[bool]
    studentkort: Optional[Studentkort]
    normalprogresjonStatus: Optional[NormalprogresjonStatus]
    undervisningstilhorighet: Optional[Undervisningstilhorighet]
    studiemaal: Optional[Studiemaal]
    studieopplegg: Optional[Studieopplegg]
    belopBetalt: Optional[int]
    annetStudiemaal: Optional[str]
    funksjonshemming: Optional[bool]
    # datoRefundert: date or datetime?
    # datoBetalt: date or datetime?
    # datoRegistreringsformEndret: date or datetime?
    # datoInnkalt: date or datetime?
    # datoKvittering: date or datetime?


class OrganisasjonsenhetRelationType(Enum):
    GODKJENT_GSK = "GODKJENT_GSK"
    ANSATT = "ANSATT"
    OVERORDNET = "OVERORDNET"
    ERSTATTES_AV = "ERSTATTES_AV"
    GEOGRAFISK = "GEOGRAFISK"
    ADMINISTRATIVT = "ADMINISTRATIVT"
    FAGANSVARLIG = "FAGANSVARLIG"
    STUDIE = "STUDIE"
    FAGLIG = "FAGLIG"
    EKSTERN = "EKSTERN"
    BETALING = "BETALING"
    STUDIESTED = "STUDIESTED"
    TILHORIGHET = "TILHØRIGHET"
    STEMMERETT = "STEMMERETT"


class OrganisasjonsenhetRelation(FsObject):
    """
    institusjon, fakultet, institutt and gruppe are populated when client is
    called with parameter refId set.  Swagger docs say refId is a boolean, but
    it's treated as true for any value, even the empty string.
    """

    href: Optional[AnyUrl]
    institusjon: Optional[int]
    fakultet: Optional[int]
    institutt: Optional[int]
    gruppe: Optional[int]
    type: Union[OrganisasjonsenhetRelationType, str]


class Institusjonsinformasjon(FsObject):
    institusjonstypekode: Optional[str]
    bynavn: Optional[str]
    regionnavn: Optional[str]
    alternativtInstitusjonsnavn: Optional[str]
    erasmusinstitusjonskode: Optional[str]
    schacHomeOrganization: Optional[str]
    godkjentBetalingssted: Optional[bool]
    fusjonert: Optional[bool]
    telefonnummerMerknad: Optional[str]


class FsUrl(FsObject):
    kode: Optional[str]
    url: Union[AnyUrl, str]


class BibsysBestillersted(FsObject):
    kode: Optional[str]
    stednavn: Optional[str]
    navn: Optional[List[Navn]]
    aktiv: Optional[bool]


class OrganisasjonsenhetDbId(FsObject):
    """
    Represents the fields in Organisasjonsenhet.id,
    which is only populated when dbId=true
    """

    href: Optional[AnyUrl]
    institusjon: int
    fakultet: int
    institutt: int
    gruppe: int


class OrganisasjonsenhetMerknad(FsObject):
    lang: str
    value: Optional[str]
    felt: Optional[str]


class Organisasjonsenhet(FsObject):
    id: Optional[OrganisasjonsenhetDbId]  # Populated when dbId = True
    href: Optional[AnyUrl]  # Populated when dbId = False
    institusjon: Optional[int]  # Populated when dbId = False
    fakultet: Optional[int]  # Populated when dbId = False
    institutt: Optional[int]  # Populated when dbId = False
    gruppe: Optional[int]  # Populated when dbId = False
    navn: Optional[List[Navn]]
    organisasjonsenheter: Optional[List[OrganisasjonsenhetRelation]]
    institusjonsinformasjon: Optional[Institusjonsinformasjon]
    adresser: Optional[List[Adresse]]
    telefoner: Optional[List[Telefon]]
    eposter: Optional[List[Epost]]
    urler: Optional[List[FsUrl]]
    land: Optional[LandRef]
    periode: Optional[Periode]
    lms: Optional[Lms]
    bibsysBestillersted: Optional[BibsysBestillersted]
    merknad: Optional[List[OrganisasjonsenhetMerknad]]
    generellMerknad: Optional[str]
    akronym: Optional[str]
    organisasjonsnivakode: Optional[str]
    organisasjonsnivanavn: Optional[str]
    nsdAvdelingskode: Optional[str]
    eiertypekode: Optional[str]
    eiertypenavn: Optional[str]
    nsdInstitusjonskode: Optional[int]
    organisasjonsnummer: Optional[int]
    organisasjonskodeKonvertering: Optional[str]
    fakultetsfarge: Optional[str]
    aktiv: Optional[bool]
    stemmerettsted: Optional[bool]
    internSensurEpost: Optional[bool]
    eksternSensurEpost: Optional[bool]
    samletKlagegruppe: Optional[bool]
    felles: Optional[bool]
