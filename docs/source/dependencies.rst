fs-client dependencies
=======================

Dependencies are maintained in a ``pyproject.toml`` file.  This file is in turn
used by installers like *pip* to fetch and setup our toolchain for installing,
building, or publishing this package.

.. important::
   Any limitations applied through requirement files should be done to prevent
   *insecure*, *buggy* or *incompatible* versions from being used.  Version
   locking should be left to any project that use this library.

.. admonition:: TODO

   Apply minimum version, prevent new major versions from being introduced
   without manual intervention.


Installation
------------
In order to install and/or otherwise use *fs-client*, the following Python
package distributions are needed:

pydantic
    Used for modelling FS objects.

    Use should be limited to :mod:`fs_client.models`.

requests
    Handles all HTTP communication with the FS API.

    Use should be limited to :mod:`fs_client.client`.


Other dependencies
------------------
Dependencies for development, testing and documentation are listed in their own
requirement files:

- Documentation: ``docs/requirements.txt``
- Testing and development: See ``pyproject.toml``
