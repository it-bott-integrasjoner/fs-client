fs_client.terms
================

Module for dealing with semesters, terms and dates.

.. automodule:: fs_client.terms
   :members:
