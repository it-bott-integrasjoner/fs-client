import requests
import pytest
import json
import os


@pytest.fixture
def http_request():
    return requests.Request()


@pytest.fixture
def base_url():
    return "https://localhost"


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def emne_data():
    return load_json_file("emne.json")


@pytest.fixture
def undervisning_data():
    return load_json_file("undervisning.json")


@pytest.fixture
def undervisningsaktivitet_data():
    return load_json_file("undervisningsaktivitet.json")


@pytest.fixture
def studentundervisning_data():
    return load_json_file("studentundervisning.json")


@pytest.fixture
def studentaktivitet_data():
    return load_json_file("studentaktivitet.json")


@pytest.fixture
def person_data():
    return load_json_file("person.json")


@pytest.fixture
def personrolle_data():
    return load_json_file("personrolle.json")


@pytest.fixture
def studentvurdering_vurdert_data():
    return load_json_file("studentvurdering_vurdert.json")


@pytest.fixture
def studentvurdering_ikke_vurdert_data():
    return load_json_file("studentvurdering_ikke_vurdert.json")


@pytest.fixture
def vurderingsresultatstatus_stryk():
    return load_json_file("vurderingsresultatstatus_s.json")


@pytest.fixture
def vurderingsresultatstatus_bestaatt():
    return load_json_file("vurderingsresultatstatus_b.json")


@pytest.fixture
def organisasjonsenhet_without_dbid():
    return load_json_file("organisasjonsenhet_without_dbid.json")


@pytest.fixture
def organisasjonsenhet_with_dbid():
    return load_json_file("organisasjonsenhet_with_dbid.json")
