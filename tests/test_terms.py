import copy
import datetime

import pytest

from fs_client.terms import Term
from fs_client.terms import Semesters
from fs_client.terms import TermIterator
from fs_client.terms import PeriodTermIterator
from fs_client.terms import term_to_shorthand, shorthand_to_term


@pytest.fixture
def terms_data():
    return (
        (1999, Semesters.AUTUMN, 1),
        (2000, Semesters.SPRING, 2),
        (2000, Semesters.AUTUMN, 3),
        (2001, Semesters.SPRING, 4),
    )


@pytest.fixture
def terms(terms_data):
    return tuple(Term(*args) for args in terms_data)


@pytest.fixture
def term_data(terms_data):
    return terms_data[0]


@pytest.fixture
def term(term_data):
    return Term(*term_data)


def test_init(term_data):
    term = Term(*term_data)
    assert term.year == term_data[0]
    assert term.semester == term_data[1]
    assert term.number == term_data[2]


def test_repr(term_data):
    term = Term(*term_data)
    r = repr(term)
    assert Term.__name__ in r
    assert str(term_data[0]) in r
    assert str(term_data[1]) in r
    assert str(term_data[2]) in r


def test_eq(term_data):
    t1 = Term(*term_data)
    t2 = Term(*term_data)
    assert t1 is not t2
    assert t1 == t2


def test_ne(terms):
    assert terms[0] != terms[1]
    assert terms[0] != terms[2]
    assert terms[0] != terms[3]


def test_copy(term):
    term_copy = copy.copy(term)
    assert term_copy is not term
    assert term_copy == term


def test_next_term(terms):
    assert terms[0].next_term() == terms[1]
    assert terms[1].next_term() == terms[2]
    assert terms[2].next_term() == terms[3]


def test_prev_term(terms):
    assert terms[3].prev_term() == terms[2]
    assert terms[2].prev_term() == terms[1]
    assert terms[1].prev_term() == terms[0]
    assert terms[0].prev_term() is None


def test_iterator(terms):
    iterator = TermIterator(terms[0])
    assert next(iterator) == terms[0]
    assert next(iterator) == terms[1]
    assert next(iterator) == terms[2]
    assert next(iterator) == terms[3]


def test_iterator_reverse(terms):
    iterator = TermIterator(terms[3], reverse=True)
    assert next(iterator) == terms[3]
    assert next(iterator) == terms[2]
    assert next(iterator) == terms[1]
    assert next(iterator) == terms[0]


def test_iterator_reverse_stop(terms):
    iterator = TermIterator(terms[0], reverse=True)
    next(iterator)  # terms[0]
    with pytest.raises(StopIteration):
        next(iterator)


def test_period_iterator():
    start = datetime.date(2017, 7, 31)
    end = "2018-12-31"
    iterator = PeriodTermIterator(start, end)
    terms = list(iterator)
    assert len(terms) == 4
    assert terms[0] == Term(2017, Semesters.SPRING, 1)
    assert terms[1] == Term(2017, Semesters.AUTUMN, 2)
    assert terms[2] == Term(2018, Semesters.SPRING, 3)
    assert terms[3] == Term(2018, Semesters.AUTUMN, 4)


def test_period_iterator_missing_start_or_end():
    start = datetime.date(2017, 7, 31)
    iterator = PeriodTermIterator(start=start, end=None)
    terms = list(iterator)
    assert len(terms) == 1
    assert terms[0] == Term(2017, Semesters.SPRING, 1)
    end = datetime.date(2018, 12, 31)
    iterator = PeriodTermIterator(start=None, end=end)
    terms = list(iterator)
    assert len(terms) == 1
    assert terms[0] == Term(2018, Semesters.AUTUMN, 1)


def test_term_to_shorthand():
    term = Term(2019, "VÅR", number=None)
    assert term_to_shorthand(term) == "19V"


def test_shorthand_to_term():
    term = Term(2019, "VÅR", number=None)
    assert shorthand_to_term("19v") == term
    assert shorthand_to_term("19V") == term
    assert shorthand_to_term("19F") is None
