import pytest

from fs_client.client import FsEndpoints


@pytest.fixture
def endpoints(base_url):
    return FsEndpoints(base_url)


def test_get_token(base_url, endpoints):
    assert endpoints.get_token() == base_url + "/token"


def test_get_emne(base_url, endpoints):
    assert endpoints.get_emne("foo") == base_url + "/emner/foo"


def test_get_emne_escape(base_url, endpoints):
    assert endpoints.get_emne("foo/bar") == base_url + "/emner/foo%2Fbar"


def test_list_emner(base_url, endpoints):
    assert endpoints.list_emner() == base_url + "/emner"


def test_get_undervisningsaktivitet(base_url, endpoints):
    expect = base_url + "/undervisningsaktiviteter/foo"
    endpoint = endpoints.get_undervisningsaktivitet("foo")
    assert endpoint == expect


def test_list_undervisningsaktiviteter(base_url, endpoints):
    expect = base_url + "/undervisningsaktiviteter"
    endpoint = endpoints.list_undervisningsaktiviteter()
    assert endpoint == expect


def test_get_undervisning(base_url, endpoints):
    expect = base_url + "/undervisning/foo"
    endpoint = endpoints.get_undervisning("foo")
    assert endpoint == expect


def test_list_undervisninger(base_url, endpoints):
    expect = base_url + "/undervisning"
    endpoint = endpoints.list_undervisninger()
    assert endpoint == expect


def test_get_studentundervisning(base_url, endpoints):
    expect = base_url + "/studentundervisning/foo"
    endpoint = endpoints.get_studentundervisning("foo")
    assert endpoint == expect


def test_list_studentundervisninger(base_url, endpoints):
    expect = base_url + "/studentundervisning"
    endpoint = endpoints.list_studentundervisninger()
    assert endpoint == expect


def test_get_studentundervisningsaktivitet(base_url, endpoints):
    expect = base_url + "/studentundervisningsaktiviteter/foo"
    endpoint = endpoints.get_studentundervisningsaktivitet("foo")
    assert endpoint == expect


def test_list_studentundervisningsaktiviteter(base_url, endpoints):
    expect = base_url + "/studentundervisningsaktiviteter"
    endpoint = endpoints.list_studentundervisningsaktiviteter()
    assert endpoint == expect


def test_get_person(base_url, endpoints):
    expect = base_url + "/personer/foo"
    endpoint = endpoints.get_person("foo")
    assert endpoint == expect


def test_list_personer(base_url, endpoints):
    expect = base_url + "/personer"
    endpoint = endpoints.list_personer()
    assert endpoint == expect


def test_get_deltaker(base_url, endpoints):
    expect = base_url + "/deltakere/foo"
    endpoint = endpoints.get_deltaker("foo")
    assert endpoint == expect


def test_list_deltakere(base_url, endpoints):
    expect = base_url + "/deltakere"
    endpoint = endpoints.list_deltakere()
    assert endpoint == expect


def test_get_personrolle(base_url, endpoints):
    expect = base_url + "/personroller/foo"
    endpoint = endpoints.get_personrolle("foo")
    assert endpoint == expect


def test_list_personroller(base_url, endpoints):
    expect = base_url + "/personroller"
    endpoint = endpoints.list_personroller()
    assert endpoint == expect


def test_get_evukurs(base_url, endpoints):
    expect = base_url + "/evukurs/foo"
    endpoint = endpoints.get_evukurs("foo")
    assert endpoint == expect


def test_list_evukurs(base_url, endpoints):
    expect = base_url + "/evukurs"
    endpoint = endpoints.list_evukurs()
    assert endpoint == expect


def test_get_evukursdeltakelse(base_url, endpoints):
    expect = base_url + "/evukursdeltakelser/foo"
    endpoint = endpoints.get_evukursdeltakelse("foo")
    assert endpoint == expect


def test_list_evukursdeltakelser(base_url, endpoints):
    expect = base_url + "/evukursdeltakelser"
    endpoint = endpoints.list_evukursdeltakelser()
    assert endpoint == expect


def test_list_studentvurderinger(base_url, endpoints):
    expect = base_url + "/studentvurderinger"
    endpoint = endpoints.list_studentvurderinger()
    assert endpoint == expect


def test_get_studentvurdering(base_url, endpoints):
    expect = base_url + "/studentvurderinger/foo"
    endpoint = endpoints.get_studentvurdering("foo")
    assert endpoint == expect


def test_get_vurderingsresultatstatuser(base_url, endpoints):
    expect = base_url + "/vurderingsresultatstatuser/foo"
    endpoint = endpoints.get_vurderingsresultatstatuser("foo")
    assert endpoint == expect


def test_get_organisasjonsenhet(base_url, endpoints):
    expect = base_url + "/organisasjonsenheter/foo"
    endpoint = endpoints.get_organisasjonsenhet("foo")
    assert endpoint == expect


def test_list_organisasjonsenheter(base_url, endpoints):
    expect = base_url + "/organisasjonsenheter"
    endpoint = endpoints.list_organisasjonsenheter()
    assert endpoint == expect
