import pytest

from fs_client.client import FsClient


@pytest.fixture
def client(base_url: str):
    return FsClient(url=base_url)


def test_rewrite_relative_urls_recursive(client: FsClient):
    # some institutions choose to remove the schema and domain from any URLs
    # contained in the response payload.
    # we this that by joining the relative URL with the configured base URL
    payload = {
        "href": "/foo/bar/baz",
        "dict": {"href": "/foo/bar/baz"},
        "list": [{"href": "/foo/bar/baz"}],
    }
    client.rewrite_relative_urls_recursive(payload, "href")
    assert payload["href"] == "https://localhost/foo/bar/baz"
    assert payload["dict"] == {"href": "https://localhost/foo/bar/baz"}
    assert payload["list"] == [{"href": "https://localhost/foo/bar/baz"}]
    # note that the relative URLs are relative to the domain, while the
    # base URL may have an additional path.
    # the behaviour of urllib.parse.urljoin makes this a non-issue
    client.urls.baseurl = "https://localhost/some/path/"
    payload["href"] = "/some/path/foo"
    client.rewrite_relative_urls_recursive(payload, "href")
    assert payload["href"] == "https://localhost/some/path/foo"
