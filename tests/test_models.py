import pytest
from fs_client import FsClient, models
from fs_client.models import (
    Aktivitet,
    AktivitetId,
    Emne,
    EmneId,
    EvuKursDeltakelseId,
    EvuKursId,
    KullKlasseStudentRef,
    Organisasjonsenhet,
    OrganisasjonsenhetDbId,
    OrganisasjonsenhetId,
    OrganisasjonsenhetRef,
    Person,
    PersonRolle,
    StudentAktivitet,
    StudentAktivitetId,
    StudentUndervisning,
    StudentVurdering,
    Undervisning,
    UndervisningId,
    VurderingsResultatStatus,
)


@pytest.fixture
def studentaktivitet_fs_id():
    return "100,ENG101,1,2018,HØST,1,2-1,1,ØVE,TEORI,1234567"


@pytest.fixture
def studentaktivitet_fs_id_dict():
    return {
        "emne_institusjon": "100",
        "emne_kode": "ENG101",
        "emne_versjon": "1",
        "semester_ar": "2018",
        "semester_termin": "HØST",
        "undervisningsenhet_terminnummer": "1",
        "undervisningsaktivitet_aktivitet": "2-1",
        "parti": "1",
        "form": "ØVE",
        "disiplin": "TEORI",
        "person_id": "1234567",
    }


def test_make_StudentAktivitetId_from_fs_id(
    studentaktivitet_fs_id, studentaktivitet_fs_id_dict
):
    sa = StudentAktivitetId.from_fs_id(studentaktivitet_fs_id)
    assert sa.dict() == studentaktivitet_fs_id_dict
    assert sa.emne_kode == studentaktivitet_fs_id_dict["emne_kode"]
    assert type(sa.aktivitet) == AktivitetId
    assert type(sa.undervisning) == UndervisningId
    assert type(sa.emne) == EmneId
    assert sa.fs_id == studentaktivitet_fs_id


def test_refs_cannot_have_empty_fields(studentaktivitet_fs_id_dict):
    bogus_id = "100,ENG101,1,2018,HØST,1,2-1,,ØVE,TEORI,1234567"
    with pytest.raises(ValueError):
        StudentAktivitetId.from_fs_id(bogus_id)
    bogus_dict = studentaktivitet_fs_id_dict
    bogus_dict["emne_kode"] = None
    with pytest.raises(ValueError):
        StudentAktivitetId(**bogus_dict)


def test_EvuKursDeltakelseRef():
    ekd = EvuKursDeltakelseId.from_fs_id("123,FOO456,2008H")
    expected_dict = {
        "deltaker_nummer": "123",
        "kode": "FOO456",
        "tidsangivelse": "2008H",
    }
    assert ekd.dict() == expected_dict
    assert type(ekd.evukurs) == EvuKursId
    del expected_dict["deltaker_nummer"]
    assert ekd.evukurs.dict() == expected_dict


def test_Emne(emne_data):
    emne = Emne.from_dict(emne_data)
    assert emne.ref.fs_id == "185,INF1000,1"


@pytest.mark.skip(reason="unhandled case")
def test_Emne_with_comma_in_code(emne_data):
    emne_comma = emne_data.copy()
    emne_comma["href"] = "https://example.org/emner/185,INF1000%2CB,1"
    emne_comma["kode"] = "INF1000,B"
    emne = Emne.from_dict(emne_comma)
    assert emne.ref.emne_kode == "INF1000,B"
    assert emne.ref.fs_id == "185,INF1000%2CB,1"


def test_Undervisning(undervisning_data):
    undervisning = Undervisning.from_dict(undervisning_data)
    assert undervisning.ref.fs_id == "185,INF1000,1,2019,HØST,1"
    assert undervisning.emne.ref.fs_id == "185,INF1000,1"


def test_Aktivitet(undervisningsaktivitet_data):
    aktivitet = Aktivitet.from_dict(undervisningsaktivitet_data)
    assert aktivitet.ref.fs_id == "185,INF1000,1,2019,HØST,1,2-16"
    assert aktivitet.ref.undervisning.fs_id == "185,INF1000,1,2019,HØST,1"
    assert aktivitet.ref.undervisning.emne.fs_id == "185,INF1000,1"


def test_StudentUndervisning(studentundervisning_data):
    su = StudentUndervisning.from_dict(studentundervisning_data)
    assert su.ref.fs_id == "185,INF1000,1,2019,HØST,1,12345"
    assert su.ref.undervisning.fs_id == "185,INF1000,1,2019,HØST,1"


def test_StudentAktivitet(studentaktivitet_data):
    sa = StudentAktivitet.from_dict(studentaktivitet_data)
    assert sa.ref.fs_id == "185,INF1000,1,2019,HØST,1,2-9,9,ØVE,TEORI,12345"
    assert sa.ref.aktivitet.fs_id == "185,INF1000,1,2019,HØST,1,2-9"
    assert sa.ref.undervisning.fs_id == "185,INF1000,1,2019,HØST,1"


def test_Person(person_data):
    person = Person.from_dict(person_data)
    assert person.personlopenummer == 12345
    assert person.epost.adresse == "olanord@example.com"
    assert person.eposter[0].type is None
    assert person.eposter[1].type == "PRIVAT"


def test_PersonRolle(personrolle_data):
    personrolle = PersonRolle.from_dict(personrolle_data)
    assert personrolle.ref.fs_id == "12222,1"
    assert personrolle.undervisning.ref.fs_id == "185,INF1000,1,2019,HØST,1"


def test_vurdert_StudentVurdering(
    studentvurdering_vurdert_data,
    studentvurdering_ikke_vurdert_data,
    vurderingsresultatstatus_bestaatt,
):
    sv = StudentVurdering.from_dict(studentvurdering_vurdert_data)
    assert sv.ref.fs_id == "12345,185,INF1000,1,OBL1,2006,HØST"
    assert sv.ref.emne_kode == "INF1000"
    assert sv.ref.vurderingskombinasjon_kode == "OBL1"
    assert sv.ref.vurderingstid_kode == "HØST"
    assert sv.ref.vurderingstid_ar == "2006"
    assert sv.erKandidat == None
    assert sv.protokoll == True
    assert sv.gyldig == True
    assert sv.vurderingsresultatstatus.ref.fs_id == "B"
    assert sv.antallBestattNgang == 1


def test_ikke_vurdert_StudentVurdering(studentvurdering_ikke_vurdert_data):
    sv = StudentVurdering.from_dict(studentvurdering_ikke_vurdert_data)
    assert sv.ref.fs_id == "12345,185,IN1000,1,OBL1,2019,HØST"
    assert sv.ref.emne_kode == "IN1000"
    assert sv.ref.vurderingskombinasjon_kode == "OBL1"
    assert sv.ref.vurderingstid_kode == "HØST"
    assert sv.ref.vurderingstid_ar == "2019"
    assert sv.erKandidat == False
    assert sv.protokoll == True
    assert sv.vurderingsresultatstatus == None
    assert sv.antallBestattNgang == None


def test_bestaat_VurderingsResultatStatus(vurderingsresultatstatus_bestaatt):
    b = VurderingsResultatStatus.from_dict(vurderingsresultatstatus_bestaatt)
    assert b.ref.fs_id == "B"
    assert b.kode == "B"
    assert b.gjentak == True
    assert b.felles == True
    assert b.prioritet == None
    assert isinstance(b.navn, list)
    assert b.navn[0].value == "Bestått"


def test_stryk_VurderingsResultatStatus(vurderingsresultatstatus_stryk):
    s = VurderingsResultatStatus.from_dict(vurderingsresultatstatus_stryk)
    assert s.ref.fs_id == "S"
    assert s.kode == "S"
    assert s.gjentak == True
    assert s.felles == True
    assert s.prioritet == 99
    assert isinstance(s.navn, list)
    assert s.navn[0].value == "Ikke bestått"


def test_relative_urls(base_url):
    """
    Tests that all relative urls are rewritten to absolute urls
    """

    api_url = "https://localhost/"
    relative_url = "/abc"
    prepend_url = api_url
    if prepend_url.endswith("/"):
        prepend_url = prepend_url[:-1]

    # Create test object with 3 layers of list/dict to test recursion
    emne: dict = {"href": relative_url}
    emne["organisasjonsenheter"] = [{"href": relative_url}, {"href": relative_url}]
    emne["organisasjonsenheter"][0]["subelements"] = [{"href": relative_url}]
    emne["organisasjonsenheter"][1]["subelements"] = [{"href": relative_url}]

    # Create a mock FsClient to test rewrite_relative_urls method
    fs = FsClient(
        url=api_url,
        headers={"X-Gravitee-Api-Key": "api-key-for-api-manager"},
        # rewrite_url rewrites hyperlinks in the objects retrieved
        rewrite_url=("https://example.org/", "https://example.com/"),
        rewrite_relative_urls=True,
    )
    fs.rewrite_relative_urls_recursive(emne, "href")

    # Check that all layers of dicts had their urls rewritten
    assert emne["href"] == "{}{}".format(prepend_url, relative_url)
    for org in emne["organisasjonsenheter"]:
        assert org["href"] == "{}{}".format(prepend_url, relative_url)
        for subelement in org["subelements"]:
            assert org["href"] == "{}{}".format(prepend_url, relative_url)


def test_OrganisasonsenhetRef():
    raw_reference = {
        "href": "https://example.org/organisasjonsenheter/185,15,5,0",
        "type": "STUDIE",
    }
    ou = OrganisasjonsenhetRef.from_dict(raw_reference)
    assert isinstance(ou.ref, OrganisasjonsenhetId)
    assert ou.ref.fs_id == "185,15,5,0"
    assert ou.ref.institusjon == "185"
    assert ou.ref.fakultet == "15"
    assert ou.ref.institutt == "5"
    assert ou.ref.gruppe == "0"


def test_Organisasjonsenhet_without_dbId(organisasjonsenhet_without_dbid):
    o = Organisasjonsenhet.from_dict(organisasjonsenhet_without_dbid)
    assert o.id is None
    assert o.institusjon == 7
    assert o.fakultet == 0
    assert o.institutt == 0
    assert o.gruppe == 0


def test_Organisasjonsenhet_with_dbId(organisasjonsenhet_with_dbid):
    o = Organisasjonsenhet.from_dict(organisasjonsenhet_with_dbid)
    assert isinstance(o.id, OrganisasjonsenhetDbId)
    assert o.id.institusjon == 7
    assert o.id.fakultet == 0
    assert o.id.institutt == 0
    assert o.id.gruppe == 0
    assert all(x is None for x in (o.institusjon, o.fakultet, o.institutt, o.gruppe))


def test_OrganisasjonsenhetId_extra_refs():
    raw_orgenhet_id = "185,15,3,10"
    orgenhet_id = OrganisasjonsenhetId.from_fs_id(raw_orgenhet_id)
    assert orgenhet_id.institutt_ref == OrganisasjonsenhetId.from_fs_id("185,15,3,0")
    assert orgenhet_id.fakultet_ref == OrganisasjonsenhetId.from_fs_id("185,15,0,0")
    assert orgenhet_id.institusjon_ref == OrganisasjonsenhetId.from_fs_id("185,0,0,0")


def test_KullKlasseStudentRef():
    d = {
        "href": "https://example.org/kullklassestudenter/HFB-ARKO,2020,H%C3%98ST,1301234,HFB-ARKO,2020,H%C3%98ST,ARK",
        "studierett": {
            "href": "https://example.org/studieretter/HFB-ARKO,2020,H%C3%98ST,1301234",
            "person": {
                "href": "https://example.org/personer/1301234",
                "personlopenummer": 1301234,
            },
            "studieprogram": {
                "href": "https://example.org/studieprogrammer/HFB-ARKO",
                "kode": "HFB-ARKO",
            },
            "termin": {
                "href": "https://example.org/semestre/2020,H%C3%98ST",
                "type": "START",
                "ar": 2020,
                "termin": "HØST",
            },
        },
        "kullklasse": {
            "href": "https://example.org/kullklasser/HFB-ARKO,2020,H%C3%98ST,ARK",
            "kull": {
                "href": "https://example.org/kull/HFB-ARKO,2020,H%C3%98ST",
                "studieprogram": {
                    "href": "https://example.org/studieprogrammer/HFB-ARKO",
                    "kode": "HFB-ARKO",
                },
                "semester": {
                    "href": "https://example.org/semestre/2020,H%C3%98ST",
                    "ar": 2020,
                    "termin": "HØST",
                },
            },
            "kode": "ARK",
        },
    }
    ref = KullKlasseStudentRef.from_dict(d)
    assert ref.studierett.person.personlopenummer == 1301234
    assert ref.studierett.studieprogram.kode == "HFB-ARKO"
    assert ref.studierett.termin.type == "START"
    assert ref.studierett.termin.ar == 2020
    assert ref.studierett.termin.termin == "HØST"
    assert ref.kullklasse.kull.studieprogram.kode == "HFB-ARKO"
    assert ref.kullklasse.kull.semester.ar == 2020
    assert ref.kullklasse.kull.semester.termin == "HØST"
    assert ref.kullklasse.kode == "ARK"
