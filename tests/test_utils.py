import pytest

from urllib.parse import quote
from fs_client.utils import extract_href, extract_id, split_id


@pytest.fixture
def fs_id():
    return "100,foo,bar,baz-bat,HØST"


@pytest.fixture
def fs_href(base_url, fs_id):
    return "{}/foo/{}".format(base_url, quote(fs_id))


@pytest.fixture
def fs_object_with_href(fs_href):
    return {"href": fs_href}


@pytest.fixture
def fs_object_href_with_singular_id(base_url):
    return extract_id("{}/foo/{}".format(base_url, quote("A")))


@pytest.fixture
def fs_object_with_self(fs_href):
    return {"self": {"href": fs_href}}


def test_extract_href(fs_href, fs_object_with_self, fs_object_with_href):
    assert extract_href(fs_object_with_href) == fs_href
    assert extract_href(fs_object_with_self) == fs_href


def test_extract_id(fs_id, fs_href, fs_object_with_href):
    assert extract_id(fs_object_with_href) == fs_id
    assert extract_id(fs_href) == fs_id


def test_split_id(fs_id, fs_object_href_with_singular_id):
    parts = ["100", "foo", "bar", "baz-bat", "HØST"]
    assert split_id(fs_id) == parts
    assert split_id("foo") == ["foo"]
    assert split_id(fs_object_href_with_singular_id) == ["A"]
