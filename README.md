# fs-client

fs-client is a Python 3.x library for accessing the [Felles Studentsystem][fs]
[API][fs-api].


## Usage

```python
from fs_client import FsClient
fs = FsClient(url='https://example.com/',
        headers={'X-Gravitee-Api-Key':
                 'api-key-for-api-manager'},
        # rewrite_url rewrites hyperlinks in the objects retrieved
        rewrite_url=('https://example.org/',
                     'https://example.com/'))

# Note that _ in param names will be rewritten as . for compability with the
# FS REST API

fs.list_emner(organisasjonsenhet_fakultet=14)
```

If FS api is setup to use relative urls you also need to add this parameter in FsClient init:

rewrite_relative_urls=True

## Documentation

See [docs/](docs/source/) for more.


  [fs]: https://www.fellesstudentsystem.no/
  [fs-api]: http://www.fellesstudentsystem.no/dokumentasjon/brukerdok/fswebservice/
